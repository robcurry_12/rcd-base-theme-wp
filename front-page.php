<?php
/**
 * Template Name: Frontpage
 *
 * The template for displaying on the frontpage
 *
 * This is the template that displays on the frontpage.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package RCD_Base
 *
 */
get_template_part( 'page' );