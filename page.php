<?php
/**
 * Default page template
 *
 * @package rcd_base/apex
 */
get_header();

if( have_posts() ) : 
    while( have_posts() ) : the_post();

        the_content();

    endwhile; /* End wordpress loop */
endif; /* End wordpress loop */

get_footer();
?>