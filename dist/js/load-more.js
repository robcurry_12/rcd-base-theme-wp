/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./assets/js/load-more.js":
/*!********************************!*\
  !*** ./assets/js/load-more.js ***!
  \********************************/
/***/ (() => {

eval("(function($, themeVars) {\r\n\r\n    $('a.load-more').on ( 'click', function(e) {\r\n        e.preventDefault();\r\n        let clickedLink  = $(this);\r\n        let postType     = $(this).data('post-type');\r\n        let postsPerPage = $(this).data('posts-per-page');\r\n        let pageNum      = $(this).data('page'); \r\n\r\n        clickedLink.addClass(\"loading\");\r\n        $.ajax({\r\n            url: themeVars.ajax.url,\r\n            type: 'POST',\r\n            data: {\r\n                action: 'load_more_posts',\r\n                nonce: themeVars.ajax.nonce,\r\n                postType : postType,\r\n                postsPerPage : postsPerPage,\r\n                page : pageNum,\r\n            },\r\n            dataType: 'json',\r\n            success: function(res) {\r\n                clickedLink.before( res.data.results );\r\n                if ( res.data.hide_load_more ) {\r\n                    clickedLink.remove();\r\n                } else {\r\n                    clickedLink.removeClass('loading');\r\n                }  \t\r\n            }\r\n        });\r\n    });\r\n\r\n})( jQuery, rcd );\n\n//# sourceURL=webpack://rcd_apex/./assets/js/load-more.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./assets/js/load-more.js"]();
/******/ 	
/******/ })()
;