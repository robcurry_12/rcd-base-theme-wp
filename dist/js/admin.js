/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./assets/js/admin.js":
/*!****************************!*\
  !*** ./assets/js/admin.js ***!
  \****************************/
/***/ (() => {

eval("( function( $ ) { \r\n    $(window).on ( 'load', function() {\r\n        $('body.wp-admin div[data-name=\"brand_colors\"] td[data-name=\"color\"]' ) .each( function( index ) { \r\n            let color = $(this).find( '.acf-input-wrap input[type=\"text\"]' ).val();\r\n            $(this).find( '.acf-input-wrap' ).css('--brand-color', color );\r\n        });\r\n        $('body.wp-admin').on( 'input', 'div[data-name=\"brand_colors\"] td[data-name=\"color\"] .acf-input-wrap input[type=\"text\"]', function() {\r\n            let color = $(this).val();\r\n            $(this).closest( '.acf-input-wrap' ).css('--brand-color', color );\r\n        });\r\n        var urlParams = new URLSearchParams(window.location.search);      \r\n        if ( urlParams.has('post_type') && urlParams.has('category') ) {\r\n            $(\"#menu-pages li.wp-first-item\").removeClass('current');\r\n            if ( urlParams.get('category') == 'landing-page' ) {\r\n                $('#menu-pages li:has(a[href=\"edit.php?post_type=page&category=landing-page\"])').addClass('current');\r\n            } else if ( urlParams.get('category') == 'website-page' ) {\r\n                $('#menu-pages li:has(a[href=\"edit.php?post_type=page&category=website-page\"])').addClass('current');\r\n            }\r\n        }  \r\n    });\r\n\r\n}) ( jQuery );\n\n//# sourceURL=webpack://rcd_apex/./assets/js/admin.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./assets/js/admin.js"]();
/******/ 	
/******/ })()
;