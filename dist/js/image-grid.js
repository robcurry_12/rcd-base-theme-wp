/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./assets/js/image-grid.js":
/*!*********************************!*\
  !*** ./assets/js/image-grid.js ***!
  \*********************************/
/***/ (() => {

eval("( function( $ ) {\r\n    \r\n    let slidesToShow = ( $('.layout--image-grid.slider').hasClass('four-columns') ) ? 4 : 3; \r\n\r\n    $('.layout--image-grid.slider .images-container').slick({\r\n        infinite: true,\r\n        loop: true,\r\n        slidesToShow: slidesToShow,\r\n        slidesToScroll: 1,\r\n        centerMode: true,\r\n        arrows: false,\r\n        dots: true,\r\n        lazyLoad: 'progressive',\r\n        cssEase: 'ease-in-out',\r\n        responsive: [\r\n        {\r\n          breakpoint: 1200,\r\n          settings: {\r\n            slidesToShow: slidesToShow,\r\n          }\r\n        },\r\n        {\r\n            breakpoint: 992,\r\n            settings: {\r\n              slidesToShow: 3,\r\n            }\r\n        },\r\n        {\r\n          breakpoint: 767,\r\n          settings: {\r\n            slidesToShow: 2,\r\n          }\r\n        },\r\n        {\r\n          breakpoint: 575,\r\n          settings: {\r\n            slidesToShow: 1,\r\n          }\r\n        }\r\n        ]\r\n      });\r\n      $(window).on( 'load resize', function(e) {\r\n        if ( $(window).width() < 768 ) {\r\n          $('.layout--image-grid.mobile-slider .images-container').slick({\r\n            infinite: true,\r\n            loop: true,\r\n            slidesToShow: 2,\r\n            slidesToScroll: 1,\r\n            centerMode: true,\r\n            arrows: false,\r\n            dots: true,\r\n            lazyLoad: 'progressive',\r\n            cssEase: 'ease-in-out',\r\n            responsive: [\r\n              {\r\n                breakpoint: 575,\r\n                settings: {\r\n                  slidesToShow: 1,\r\n                }\r\n              }\r\n              ]\r\n          });\r\n        } else {\r\n          $('.layout--image-grid.mobile-slider .images-container').slick('unslick');\r\n        }\r\n      });\r\n\r\n}) ( jQuery );\n\n//# sourceURL=webpack://rcd_apex/./assets/js/image-grid.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./assets/js/image-grid.js"]();
/******/ 	
/******/ })()
;