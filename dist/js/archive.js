/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./assets/js/archive.js":
/*!******************************!*\
  !*** ./assets/js/archive.js ***!
  \******************************/
/***/ (() => {

eval("( function ( $ ) { \r\n\r\n    $('body.post-type-archive-rcd_work select').on( 'change', function(e) {\r\n\r\n        let filterBy = $(this).attr( 'name' );\r\n        let workType = $(this).val();\r\n        if ( workType != '' ) {\r\n            $('.work-container .single-work:not([data-' + filterBy + '*=\"|' + workType + '|\"])').slideUp();\r\n            $('.work-container .single-work[data-' + filterBy + '*=\"|' + workType + '|\"]' ).slideDown();\r\n        } else {\r\n            $('.work-container .single-work').slideDown();\r\n        }\r\n\r\n    });\r\n\r\n})( jQuery );\n\n//# sourceURL=webpack://rcd_apex/./assets/js/archive.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./assets/js/archive.js"]();
/******/ 	
/******/ })()
;