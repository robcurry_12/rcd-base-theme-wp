<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimal-ui, minimum-scale=1.0, maximum-scale=1.0"/>
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title><?php echo get_bloginfo( 'name' ); ?></title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <?php do_action( 'rcd_site_head'); ?>

</head>
<body <?php body_class(); ?>>

    <?php do_action( 'rcd_body_before' ); ?>

    <header class="site-header">
        <?php get_template_part( 'template-parts/nav/navigation' ); ?>
    </header>
    
    <?php get_template_part( 'template-parts/partials/page-header' ); ?>

    <div id="site-content">
