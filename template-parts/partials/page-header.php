<?php
/**
 * Page header that appears at the top of every page
 */

 if ( is_home() || ( is_single() && get_post_type() == 'post' ) ) :
    $post_id = get_option( 'page_for_posts' );
    $title_elem = 'p';
elseif ( get_post_type() == 'rcd_news' ) :
    $post_id = 'news-settings';
    if ( is_single() ) :
        $title_elem = 'p';
    else :
        $title_elem = 'h1';
    endif;
else :
    global $post;
    $post_id = $post->ID;
    $title_elem = 'h1';
endif;

$show_page_header = get_field( 'show_page_header', $post_id );
$hide_page_title  = get_field( 'hide_page_title', $post_id );
$use_custom_title = get_field( 'use_custom_title', $post_id );
$custom_title     = get_field( 'custom_title', $post_id );
$custom_bg_image  = get_field( 'custom_background_image', $post_id );
$turn_off_overlay = get_field( 'turn_off_overlay', $post_id );

if ( $show_page_header ) :
    ?>
    <header class="full-padding-top full-padding-bottom page-header <?php echo ( $custom_bg_image ) ? 'bg-image ' : 'no-bg-image '; echo ( $hide_page_title ) ? 'no-page-title ' : 'show-page-title '; echo ( $turn_off_overlay ) ? 'no-overlay' : ''; ?>" <?php echo ( $custom_bg_image ) ? 'style="--background-image: url(' . $custom_bg_image . ');"' : ''; ?>>
        <div class="rcd-container">
            <?php
                if ( ! $hide_page_title ) :
                    if ( $use_custom_title ) :
                        echo '<' . $title_elem . '>' . $custom_title . '</' . $title_elem . '>';
                    else :
                        echo '<' . $title_elem . '>' . get_the_title( $post_id ) . '</' . $title_elem . '>';
                    endif;
                else :
                    echo '<' . $title_elem . ' class="sr-only">' . get_the_title( $post_id ) . '</' . $title_elem . '>';
                endif;
            ?>
        </div>
    </header>
    <?php
    if ( is_single() ) :
        $post_type_obj = get_post_type_object( get_post_type() ); ?>
        <div class="back-to">
            <div class="container">
                <a href="<?php echo get_post_type_archive_link( get_post_type() ); ?>"><i class="fa-solid fa-circle-arrow-left"></i> Back to <?php echo $post_type_obj->labels->name; ?></a>
            </div>
        </div>
        <?php
    endif;
else :
    the_title( '<h1 class="sr-only">', '</h1>' );
endif;