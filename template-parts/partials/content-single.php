<?php
/**
 * Partial for displaying a single blog post or news post
 * Usually appearing on the blog page or news archive page
 */

 ?>
 <article class="single-post">
    <?php if ( ! is_single() ) : ?>
        <a href="<?php the_permalink(); ?>">
    <?php endif; ?>
        <div class="content-container">
            <?php if ( get_post_type() !== 'rcd_news' ) : ?>
                <div class="image-container">
                    <?php 
                        if ( get_the_post_thumbnail() ) :
                            the_post_thumbnail( );
                        else :
                            echo wp_get_attachment_image( get_field( 'placeholder_image', 'option' ), [1600, 900], false, ['class' => 'placeholder-image' ] );
                        endif;
                    ?>
                </div>
            <?php endif; ?>
            <div class="text-container">
                <?php 
                    if ( is_single() ) :
                        $title_elem = 'h1';
                    else :
                        $title_elem = 'p';
                    endif;
                    the_title( '<' . $title_elem . ' class="post-title h2">', '</' . $title_elem . '>' );
                    if ( get_the_author() ) :
                    ?>
                        <p class="author-date">
                            Posted by <?php echo get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name'); ?> | <?php echo get_the_date( 'F j, Y' ); ?>
                        </p>
                    <?php
                endif;
                if ( is_single() ) :
                    ?>
                    <div class="share-container">
                        <p>Share This Article</p>
                        <a href="https://twitter.com/share?url=[<?php the_permalink(); ?>&text=<?php the_title(); ?>" class="share-link twitter" target="_blank"><i class="fa-brands fa-twitter"></i></a>
                        <a href="https://www.linkedin.com/shareArticle?url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" class="share-link linkedin" target="_blank"><i class="fa-brands fa-linkedin"></i></a>
                        <a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" class="share-link facebook" target="_blank"><i class="fa-brands fa-facebook"></i></a>
                    </div>
                    <?php
                    the_content();
                else :
                    ?>
                    <p><?php echo get_the_excerpt( ); ?></p>
                    <span class="btn btn-solid brand-color-1">Read More</span>
                    <?php
                endif;
                ?>

            </div>

        </div>
    <?php if ( ! is_single() ) : ?>
        </a>
    <?php endif; ?>
 </article>