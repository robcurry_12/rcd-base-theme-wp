<?php
/**
 * 
 * Content block displaying an 
 * Instagram feed configured in
 * Smash Balloon plugin
 *
 */
 ?>
<section <?php rcd_block_settings( 'instagram-feed', [] );?> >

<div class="rcd-container">

    <?php rcd_block_heading(); ?>
    
    <div class="shortcode-container">

        <?php
            if ( get_field( 'shortcode' ) ) :
                if ( is_admin() ) :
                    echo '<h2>The Instagram feed cannot render in the block editor</h2>';
                else :
                    echo do_shortcode( get_field( 'shortcode' ) );
                endif;
            else :
                if ( is_admin() ) :
                    echo '<h2>No shortcode has been provided</h2>';
                endif;
            endif;
        ?>

    </div>

</div>

</section>