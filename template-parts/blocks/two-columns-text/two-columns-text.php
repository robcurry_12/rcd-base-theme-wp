<?php
/**
 * 
 * Two column text only content block 
 *
 */

 $column_width = get_field( 'column_width' );
 $font_size    = get_field( 'font_size' );

 ?>
<section <?php rcd_block_settings( 'two-columns-text', [ $column_width, $font_size ] );?> >

<div class="rcd-container">

    <?php rcd_block_heading(); ?>
    
    <div class="columns-container">
        <div class="left-column entry-content">
            <?php
                if ( get_field( 'left_column' ) ) :
                    echo apply_filters( 'the_content', get_field( 'left_column' ) );
                endif;
            ?>
        </div>
        <div class="right-column entry-content">
            <?php
                if ( get_field( 'right_column' ) ) :
                    echo apply_filters( 'the_content', get_field( 'right_column' ) );
                endif;
            ?>
        </div>
    </div>

</div>

</section>