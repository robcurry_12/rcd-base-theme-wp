<?php
/**
 * Block that displays media in a masonry
 */

 ?>
<section <?php rcd_block_settings( 'media-masonry', [ ] );?> >

    <div class="rcd-container">

        <?php rcd_block_heading(); ?>
        
        <?phP

            if ( have_rows( 'media' ) ) :
                ?>
                <div class="media-container">
                    <?php
                        while ( have_rows( 'media' ) ) :
                            the_row();
                            ?>
                            <div class="single-media <?php echo get_sub_field( 'width' );  echo ' ' . get_sub_field( 'height' ); echo ( get_sub_field( 'type' ) == 'video' ) ? ' ' . get_sub_field( 'video' )['aspect_ratio'] : '';?>">
                                <?php 
                                    if ( get_sub_field( 'type' ) == 'video' ) : 
                                        $video = get_sub_field( 'video' )['video'];
                                        switch ( $video['type'] ) :
                                            case 'iframe':
                                                echo $video['embed_code' ];
                                                break;
                                            case 'file':
                                                $url = $video['file'];
                                                break;
                                            case 'url':
                                                $url = $video['url'];
                                                break;
                                        endswitch;
                
                                        if ( $video['type'] != 'iframe' ) :
                                            if ( $video['poster'] ) :
                                                $poster = wp_get_attachment_url( $video['poster'] );
                                            else :
                                                $poster = '';
                                            endif;
            
                                            $class = 'wp-video-shortcode';
                                            if ( ! $video['controls'] ) :
                                                $class .= ' no-controls';
                                            endif;
            
                                            echo rcd_video_shortcode( array(
                                                'src'      => $url,
                                                'poster'   => $poster,
                                                'loop'     => $video['loop'],
                                                'autoplay' => $video['autoplay'],
                                                'muted'    => $video['muted'],
                                                'class'    => $class ,
                                            ) );
                                        endif;
                                    else :
                                        echo wp_get_attachment_image( get_sub_field( 'image' ), 'full' );
                                    endif;                                
                                ?>
                            </div>
                            <?php
                        endwhile;
                    ?>
                </div><!-- .images-container-->
                <?php
            endif;
        ?>

    </div>

</section>