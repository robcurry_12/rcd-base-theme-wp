<?php
/**
 * Mini Billboard template that is just basically
 * the billboard 
 */
$media_order       = get_sub_field( 'billboard_content_order' );
$image_size        = get_sub_field( 'billboard_image_size' );
$content_alignment = get_sub_field( 'billboard_content_alignment');
$multiple_images   = ( get_sub_field( 'billboard_multiple_images' ) ) ? 'billboard_multiple-images': '';

?>
<section <?php rcd_block_settings( 'billboard', [ $media_order, $image_size, $content_alignment, $multiple_images ] );?> >

    <div class="rcd-container">
        
        <div class="content-container">
            <div class="image-container">
                <?php
                    if ( $multiple_images ) :
                        $images = get_sub_field( 'billboard_images' );
                        if ( is_countable( $images ) && count( $images ) > 0 ) :
                            foreach ( $images as $image ) :
                                echo wp_get_attachment_image( $image, 'billboard-image' );
                            endforeach;
                        endif;
                    else :
                        if ( get_sub_field( 'billboard_image' ) ) :
                            echo wp_get_attachment_image( get_sub_field( 'image' ), 'billboard-image' );
                        endif;
                    endif;
                ?>

            </div>
            <div class="text-container">
                <?php if ( get_sub_field( 'billboard_pre_headline' ) ) : ?>
                    <p class="billboard-pre-headline lg-p"><?php the_sub_field( 'billboard_pre_headline' ); ?></p>
                <?php endif; ?>
                <?php if ( get_sub_field( 'billboard_headline' ) ) : ?>
                    <h2 class="billboard-headline"><?php the_sub_field( 'billboard_headline' ); ?></h2>
                <?php endif; ?>
                <?php if ( get_sub_field( 'billboard_subtitle' ) ) : ?>
                    <h3 class="billboard-subtitle h4"><?php the_sub_field( 'billboard_subtitle' ); ?></h3>
                <?php endif; ?>

                <?php 
                    if ( get_sub_field( 'billboard_content') ) :
                        echo apply_filters( 'the_content', get_sub_field( 'billboard_content' ) );
                    endif;

                    if ( get_sub_field( 'link' ) ) :
                        rcd_call_to_action_partial( array(
                            'link' => get_sub_field( 'link' ),
                            'cta_color' => get_sub_field( 'cta_color' ),
                            'cta_type'  => get_sub_field( 'cta_type' )
                        ) );
                    endif;
                ?>
            </div>
        </div><!-- .content-container -->

    </div>

</section>