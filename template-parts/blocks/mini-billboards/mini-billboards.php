<?php
/**
 * 
 * Mini Billboards - currently only supports images
 * Each billboard will take up half the container 
 */

?>
<section <?php rcd_block_settings( 'mini-billboards', [] );?> >

    <div class="rcd-container">

        <?php rcd_block_heading(); ?>
        
        <?php if ( have_rows( 'billboards' ) ) : ?>

            <div class="billboards-container">

                <?php
                    while ( have_rows ( 'billboards' ) ) :
                        the_row();
                        get_template_part( 'template-parts/blocks/mini-billboards/mini-billboard' );
                    endwhile;
                ?>

            </div><!--billboards-container -->

        <?php endif; ?>

    </div>

</section>