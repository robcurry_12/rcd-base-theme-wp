<?php
/**
 * 
 * Billboard - currently only supports images
 * 
 */
 
$media_order       = get_field( 'content_order' );
$image_size        = get_field( 'image_size' );
$content_alignment = get_field( 'content_alignment');
$media_type        = get_field( 'media_type' );
$multiple_images   = ( get_field( 'multiple_images' ) ) ? 'multiple-images': '';

?>
<section <?php rcd_block_settings( 'billboard', [ $media_order, $image_size, $content_alignment, $multiple_images, $media_type ] );?> >

    <div class="rcd-container">

        <?php rcd_block_heading(); ?>
        
        <div class="content-container">
            <?php if ( $media_type != 'video-billboard' ) : ?>
                <div class="image-container">
                    <?php
                        if ( $multiple_images ) :
                            $images = get_field( 'images' );
                            if ( is_countable( $images ) && count( $images ) > 0 ) :
                                foreach ( $images as $image ) :
                                    echo wp_get_attachment_image( $image, 'billboard-image' );
                                endforeach;
                            endif;
                        else :
                            if ( get_field( 'image' ) ) :
                                echo wp_get_attachment_image( get_field( 'image' ), 'billboard-image' );
                            endif;
                        endif;
                    ?>

                </div>
            <?php else : ?>
                <div class="video-container <?php echo esc_attr( get_field( 'aspect_ratio') ); echo ( get_field( 'iphone_video_overlay' ) ) ? ' iphone-overlay' : ''; ?>">
                    <?php if ( is_group_filled( get_field( 'billboard_video' ) ) ) :
                            $video = get_field( 'billboard_video' )['video'];

                            switch ( $video['type'] ) :
                                case 'iframe':
                                    echo $video['embed_code'];
                                    break;
                                case 'file':
                                    $url = $video['file'];
                                    break;
                                case 'url':
                                    $url = $video['url'];
                                    break;
                            endswitch;

                            if ( $video['type'] != 'iframe' ) :
                                if ( $video['poster'] ) :
                                    $poster = wp_get_attachment_url( $video['poster'] );
                                else :
                                    $poster = '';
                                endif;

                                $class = 'wp-video-shortcode';
                                if ( ! $video['controls'] ) :
                                    $class .= ' no-controls';
                                endif;

                                echo rcd_video_shortcode( array(
                                    'src'      => $url,
                                    'poster'   => $poster,
                                    'loop'     => $video['loop'],
                                    'autoplay' => $video['autoplay'],
                                    'muted'    => $video['muted'],
                                    'class'    => $class ,
                                ) );
                            endif;
                        endif;
                    ?>


                </div>
            <?php endif; ?>
            <div class="text-container">
                <?php if ( get_field( 'pre_headline' ) ) : ?>
                    <p class="billboard-pre-headline lg-p"><?php the_field( 'pre_headline' ); ?></p>
                <?php endif; ?>
                <?php if ( get_field( 'headline' ) ) : ?>
                    <h2 class="billboard-headline"><?php the_field( 'headline' ); ?></h2>
                <?php endif; ?>
                <?php if ( get_field( 'subtitle' ) ) : ?>
                    <h3 class="billboard-subtitle h4"><?php the_field( 'subtitle' ); ?></h3>
                <?php endif; ?>

                <?php 
                    if ( get_field( 'content') ) :
                        echo apply_filters( 'the_content', get_field( 'content' ) );
                    endif;
                    rcd_call_to_action_partial( get_field( 'call_to_action' ) );
                ?>
            </div>
        </div><!-- .content-container -->

    </div>

</section>