<?php
/**
 * 
 * Content block displaying 
 * social media links and 
 * their corresponding icons
 *
 */
 ?>
<section <?php rcd_block_settings( 'social-media-icons', [] );?> >

<div class="rcd-container">

    <?php rcd_block_heading(); ?>
    
    <?php if ( have_rows( 'social_media_accounts' ) ) : ?>
        <div class="social-media-accounts-container">
                <?php while( have_rows( 'social_media_accounts' ) ) : the_row(); ?>
                    <div class="single-account">
                        <a href="<?php echo esc_url( get_sub_field( 'account_url' ) ); ?>" target="_blank" title="<?php echo esc_attr( get_sub_field( 'account_name' ) ); ?> on <?php echo esc_attr( get_sub_field( 'account' ) ); ?>"<?php if ( get_sub_field( 'icon_color' ) ) : ?>style="--icon-color:<?php echo get_sub_field( 'icon_color' ); ?>;"<?php endif; ?>>
                            <?php echo ( get_sub_field( 'font_awesome_code' ) ) . ' ' . get_sub_field( 'account' ); ?>
                        </a>
                    </div>
                <?php endwhile; ?>
            </ul>
        </div>
    <?php endif; ?>

</div>

</section>