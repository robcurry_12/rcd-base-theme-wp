<?php
/**
 * Video block
 */

 $aspect_ratios = get_field( 'video_aspect_ratio' );
 $video_fit     = get_field( 'video_fit' );
 $width         = get_field( 'width' );
 ?>

<section <?php rcd_block_settings( 'video', [ $width, $video_fit, $aspect_ratios ] );?> >

    <div class="rcd-container">

        <?php
            rcd_block_heading();
        
            if ( get_field( 'type' ) ) : 
                ?>
                <div class="video-container">
                    <?php
                        switch ( get_field( 'type' ) ) :
                            case 'iframe':
                                echo get_field( 'embed_code' );
                                break;
                            case 'file':
                                $url = get_field( 'file' );
                                break;
                            case 'url':
                                $url = get_field( 'url' );
                                break;
                        endswitch;

                        if ( get_field( 'type' ) != 'iframe' ) :
                            if ( get_field( 'poster' ) ) :
                                $poster = wp_get_attachment_url( get_field( 'poster' ) );
                            else :
                                $poster = '';
                            endif;

                            $class = 'wp-video-shortcode';
                            if ( ! get_field( 'controls' ) ) :
                                $class .= ' no-controls';
                            endif;

                            echo rcd_video_shortcode( array(
                                'src'      => $url,
                                'poster'   => $poster,
                                'loop'     => get_field( 'loop' ),
                                'autoplay' => get_field( 'autoplay' ),
                                'muted'    => get_field( 'muted' ),
                                'class'    => $class ,
                            ) );
                        endif;
                    ?>
                </div>
                <?php if ( get_field( 'caption' ) ) : ?>
                    <p class="video-caption"><?php echo wp_kses_post( get_field( 'caption' ) ); ?></p>
                    <?php
                endif;
            endif;
            ?> 

    </div>

</section>