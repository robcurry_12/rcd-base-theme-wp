<?php
/**
 * Video grid block displaying
 * videos of either uniform aspect ratios
 * or mixed aspect ratios
 */

 $aspect_ratios = get_field( 'video_aspect_ratio' );
 ?>

<section <?php rcd_block_settings( 'video-carousel', [ $aspect_ratios ] );?> >

    <div class="rcd-container">

        <?php rcd_block_heading(); ?>

    </div>
        
    <?php if ( have_rows( 'videos' ) ) : ?>
        <div class="video-carousel-container">
            <?php
                while ( have_rows( 'videos' ) ) :
                    the_row();
                    ?>
                    <div class="single-video">
                        <?php
                            switch ( get_sub_field( 'type' ) ) :
                                case 'iframe':
                                    echo get_sub_field( 'embed_code' );
                                    break;
                                case 'file':
                                    $url = get_sub_field( 'file' );
                                    break;
                                case 'url':
                                    $url = get_sub_field( 'url' );
                                    break;
                            endswitch;
    
                            if ( get_sub_field( 'type' ) != 'iframe' ) :
                                if ( get_sub_field( 'poster' ) ) :
                                    $poster = wp_get_attachment_url( get_sub_field( 'poster' ) );
                                else :
                                    $poster = '';
                                endif;

                                $class = 'wp-video-shortcode';
                                if ( ! get_sub_field( 'controls' ) ) :
                                    $class .= ' no-controls';
                                endif;

                                echo rcd_video_shortcode( array(
                                    'src'      => $url,
                                    'poster'   => $poster,
                                    'loop'     => get_sub_field( 'loop' ),
                                    'autoplay' => get_sub_field( 'autoplay' ),
                                    'muted'    => get_sub_field( 'muted' ),
                                    'class'    => $class ,
                                ) );
                            endif;
                        if ( get_sub_field( 'caption' ) ) :
                            ?>
                            <p class="video-caption"><?php echo wp_kses_post( get_sub_field( 'caption' ) ); ?></p>
                            <?php
                        endif;
                        ?>

                    </div>
                    <?php
                endwhile;
            ?>
        </div>
    <?php endif; ?>

</section>