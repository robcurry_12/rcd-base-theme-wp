<?php
/**
 * Image block
 */

 $width         = get_field( 'width' );
 ?>

<section <?php rcd_block_settings( 'image', [ $width ] );?> >

    <div class="rcd-container">

        <?php
            rcd_block_heading();
        
            if ( get_field( 'image' ) ) :
                
                echo wp_get_attachment_image( get_field( 'image' ), 'full' );
                
            endif;
            ?>
    </div>

</section>