<?php
/**
 * Partial used apart of the Baseball cards 
 */
?>

<div class="card baseball-card">
    <div class="content-container">
        <?php
            if ( $image = get_sub_field( 'image' ) ) :
                ?>
                <div class="image-container">
                    <?php
                        echo wp_get_attachment_image( $image, 'baseball-card' );
                        if ( $headline = get_sub_field( 'headline' ) ) : 
                            ?>
                            <h2 class="card-headline"><?php echo esc_attr( $headline ); ?></h2>
                            <?php
                        endif;
                    ?>
                </div>
                <?php
            endif;

            if ( $content = get_sub_field( 'content' ) ) :
                ?>
                <div class="card-content">
                    <?php echo wp_kses_post( $content ); ?>
                </div>
                <?php
            endif;
            $cta = [
                'link' => get_sub_field( 'link' ),
                'cta_color' => get_sub_field( 'cta_color' ),
                'cta_type' => get_sub_field( 'cta_type' )
            ];
            rcd_call_to_action_partial( $cta );
        ?>
    </div>
</div>