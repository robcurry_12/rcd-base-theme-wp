<?php
/**
 * Block for Baseball cards. 
 */
?>
<section <?php rcd_block_settings( 'baseball-cards', [ 'cards' ] );?> >

    <div class="rcd-container">

        <?php rcd_block_heading(); ?>
        
        <?php if ( get_field( 'cards' ) ) : ?>
            <div class="cards-container">
                <?php
                    while( have_rows( 'cards' ) ) : the_row();
                        get_template_part( 'template-parts/blocks/baseball-cards/baseball-card' );
                    endwhile;
                ?>
            </div>
        <?php endif; ?>
    </div>

</section>