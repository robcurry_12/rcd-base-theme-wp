<?php
/**
 * 
 * Block that contains the classic editor. Used for 
 * outputting basic text content
 * 
 */
?>
<section <?php rcd_block_settings( 'classic-editor', [ '' ] );?> >

    <div class="rcd-container">

        <?php rcd_block_heading(); ?>
        
        <div class="entry-content">
            <?php
                if ( get_field( 'content' ) ) :
                    echo get_field( 'content' );
                elseif( is_admin() ) :
                    echo '<h2>Enter content in the sidebar</h2>';
                endif;

                if ( $link = get_field( 'content_cta' ) ) :
                    if ( $link['link'] ) : ?>
                        <a href="<?php echo esc_url( $link['link']['url'] ); ?>" class="btn <?php echo esc_attr( $link['cta_color'] ) . ' ' . esc_attr( $link['cta_type'] ); ?>" target="<?php echo esc_attr( $link['link']['target'] ); ?>">
                            <?php echo esc_html( $link['link']['title'] ); ?>
                        </a>
                        <?php
                    endif;
                endif;
            ?>
        </div>

    </div>

</section>