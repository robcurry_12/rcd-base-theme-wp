<?php
/**
 * Block that displays images in a grid
 */

 $grid_type          = get_field( 'image_grid_type' );
 $number_of_columns  = get_field( 'image_grid_columns');
 $image_width        = get_field( 'image_width' );
 $grid_slider        = ( get_field( 'grid_slider' ) ) ? 'slider' : '';
 $grid_slider_mobile = ( get_field( 'grid_slider_mobile' ) ) ? 'mobile-slider' : '';

 ?>
<section <?php rcd_block_settings( 'image-grid', [ $grid_type, $number_of_columns, $image_width, $grid_slider, $grid_slider_mobile ] );?> >

    <div class="rcd-container">

        <?php rcd_block_heading(); ?>
        
        <?php
            if ( $grid_type == 'images-only' ) :
                $images = get_field( 'grid' );
            elseif ( $grid_type == 'linked-images' ) :
                $images = get_field( 'images' );
            endif;

            if ( ! empty( $images ) ) :
                ?>
                <div class="images-container">
                    <?php
                        foreach ( $images as $image ) :
                            switch ( $grid_type ) :
                                case 'images-only':
                                    ?>
                                    <div class="single-image">
                                        <?php echo wp_get_attachment_image( $image, [400, 400] ); ?>
                                    </div>
                                    <?php
                                    break;
                                case 'linked-images':
                                    $link = $image['link'];
                                    $img  = $image['image'];
                                    if ( $link ) : 
                                        ?>
                                        <a href="<?php echo esc_url( $link['url'] ); ?>" target="<?php echo esc_attr( $link['target'] ); ?>" class="single-linked-image">
                                        <?php
                                    else :
                                        ?>
                                        <div class="single-linked-image">
                                        <?php
                                    endif;
                                    
                                    echo wp_get_attachment_image( $img, [400, 400] );
                                    if ( $link ) : 
                                        ?>
                                        </a>
                                        <?php
                                    else :
                                        ?>
                                        </div>
                                        <?php
                                    endif;
                                    break;
                            endswitch;
                        endforeach;
                    ?>
                </div><!-- .images-container-->
                <?php
            endif;
        ?>

    </div>

</section>