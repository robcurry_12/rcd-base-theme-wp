<?php
/**
 * Block that displays images in a masonry
 */

 ?>
<section <?php rcd_block_settings( 'image-masonry', [ ] );?> >

    <div class="rcd-container">

        <?php rcd_block_heading(); ?>
        
        <?phP

            if ( have_rows( 'images' ) ) :
                ?>
                <div class="images-container">
                    <?php
                        while ( have_rows( 'images' ) ) :
                            the_row();
                            ?>
                            <div class="single-image <?php echo get_sub_field( 'width' ); ?>">
                                <?php echo wp_get_attachment_image( get_sub_field( 'image' ), 'full' ); ?>
                            </div>
                            <?php
                        endwhile;
                    ?>
                </div><!-- .images-container-->
                <?php
            endif;
        ?>

    </div>

</section>