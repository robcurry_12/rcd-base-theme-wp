<?php
/**
 * Custom Hero Block with multiple styling options
 */

 $addtl_classes   = array();
 $media_type      = get_field( 'media_type' );
 $black_overlay   = get_field( 'black_overlay' );
 $parallax        = get_field( 'parallax' );
 
 $text_alignment  = get_field( 'text_alignment' );
 $text_decoration = get_field( 'text_container' ); 

 $addtl_classes[] = 'media-type-' . $media_type;
 $addtl_classes[] = ( $black_overlay ) ? 'overlay-true' : '';
 $addtl_classes[] = 'text-alignment-' . $text_alignment;
 $addtl_classes[] = ( $text_decoration ) ? 'text-container-true' : ''; 
 $addtl_classes[] = ( $parallax ) ? 'parallax-true' : ''; 

?>

<section <?php rcd_block_settings( 'hero', $addtl_classes );?> style="<?php echo ( $media_type == 'image' ) ? '--background-image: url(' . get_field( 'hero_image' ) .');' : ''; echo ( $media_type == 'solid-color' ) ? '--background-color:' . get_field( 'hero_bg_color' ) : ''; ?>" >
    
    <?php
        if ( $media_type == 'video' ) :
            if ( is_group_filled( get_field( 'hero_video' ) ) ) :
                $hero_video = get_field( 'hero_video' );
                switch ( $hero_video['video_type'] ) :
                    case 'iframe':
                        echo $hero_video[ 'iframe_code' ];
                        break;
                    case 'file':
                        $url = $hero_video[ 'file' ];
                        break;
                    case 'url':
                        $url = $hero_video[ 'url' ];
                        break;
                endswitch;

                if ( $hero_video['video_type'] != 'iframe' ) :
                    if ( $hero_video['poster'] ) :
                        $poster = wp_get_attachment_url( $hero_video['poster'] );
                    else :
                        $poster = '';
                    endif;

                    $class = 'wp-video-shortcode';
                    if ( ! $hero_video['controls'] ) :
                        $class .= ' no-controls';
                    endif;

                    echo rcd_video_shortcode( array(
                        'src'      => $url,
                        'poster'   => $poster,
                        'loop'     => $hero_video['loop'],
                        'autoplay' => $hero_video['autoplay'],
                        'muted'    => $hero_video['muted'],
                        'class'    => $class ,
                    ) );
                endif;


            endif;
            


        endif;
    ?>

    <div class="rcd-container">

        <div class="text-container">

            <?php if ( $pre_title = get_field( 'pre_title' ) ) : ?>
                <p class="h3"><?php echo wp_kses_post( $pre_title ); ?></p>
            <?php endif; ?>

            <?php if ( $title = get_field( 'title' ) ) : ?>
                <h1><?php echo wp_kses_post( $title ); ?></h1>
            <?php endif; ?>

            <?php if ( $subtitle = get_field( 'subtitle' ) ) : ?>
                <h2 class="h5"><?php echo wp_kses_post( $subtitle ); ?></h2>
            <?php endif; ?>

            <?php if ( $content = get_field( 'content' ) ) : ?>
                <div class="content-container">
                    <?php echo wp_kses_post( $content ); ?>
                </div>
            <?php endif; ?>

            <?php if ( $cta = get_field( 'call_to_action' ) ) : ?>
                <a href="<?php echo esc_url( $cta['url'] ); ?>" target="<?php echo esc_attr( $cta['target'] ); ?>" class="btn btn-solid brand-color-2">
                    <?php echo esc_html( $cta['title'] ); ?>
                </a>
            <?php endif; ?>
        </div>

    </div>
</section>