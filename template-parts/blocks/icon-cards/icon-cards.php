<?php
/**
 * Block for floating cards. 
 */

$card_columns = get_field( 'card_columns' );
?>
<section <?php rcd_block_settings( 'icon-cards', [ 'cards', $card_columns ] );?> >

    <div class="rcd-container">

        <?php rcd_block_heading(); ?>
        
        <?php if ( get_field( 'cards' ) ) : ?>
            <div class="cards-container">
                <?php
                    while( have_rows( 'cards' ) ) : the_row();
                        get_template_part( 'template-parts/blocks/icon-cards/icon-card' );
                    endwhile;
                ?>
            </div>
            <?php
        endif;
        if ( $cta = get_field( 'call_to_action' ) ) :
            rcd_call_to_action_partial( $cta );
        endif;
        ?>

    </div>

</section>