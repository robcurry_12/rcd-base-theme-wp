<?php
/**
 * Partial used apart of the Icon cards 
 */
?>

<div class="card icon-card">
    <div class="content-container">
        <?php

            if ( $icon = get_sub_field( 'image' ) ) :
                echo wp_get_attachment_image( $icon, 'icon-card' );
            endif;

            if ( $title = get_sub_field( 'title' ) ) : 
                ?>
                <h2 class="card-title"><?php echo esc_attr( $title ); ?></h2>
                <?php
            endif;

            if ( $content = get_sub_field( 'content' ) ) :
                ?>
                <div class="card-content">
                    <?php echo wp_kses_post( $content ); ?>
                </div>
                <?php
            endif;

            $link  = get_sub_field( 'call_to_action' );
            if ( $link ) :
                ?>
                <a href="<?php echo esc_url( $link['url'] ); ?>" target="<?php echo esc_attr( $link['target'] ); ?>" class="btn btn-solid brand-color-2"><?php echo esc_html( $link['title'] ); ?></a>
                <?php
            endif;
        ?>
    </div>
</div>