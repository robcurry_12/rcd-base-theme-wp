<?php
/**
 * Block for displaying blog posts in a card format. 
 */
 $autopopulate       = get_field( 'autopopulate_posts' );
 $autopopulate_topic = ( $autopopulate ) ? get_field( 'autopopulate_topic' ) /*'apples'*/ : 'bananas';
 $posts_per_page     = ( $autopopulate ) ? get_field( 'number_of_posts' ) : 1;

 $args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
 );

 if ( $autopopulate ) :

    if ( is_array( $autopopulate_topic ) ) :
        $args['category__and'] = $autopopulate_topic;
    endif;

    $args['posts_per_page'] = $posts_per_page;
    $args['orderby'] = 'date';

else :

    $posts = get_field( 'blog_posts' );

    $args['post__in'] = ( $posts ) ? $posts : array( -1 );
    $args['orderby']  = 'post__in';

endif;

    $blogs_query = new WP_Query( $args );

    if ( $blogs_query->have_posts() ) :
        ?>
        <section <?php rcd_block_settings( 'blog-posts', [ 'cards' ] );?> >

            <div class="rcd-container">

                <?php rcd_block_heading(); ?>
            
                <div class="posts-container cards-container">
                    <?php
                        while( $blogs_query->have_posts() ) : $blogs_query->the_post();
                            get_template_part( 'template-parts/blocks/blog-posts/blog-post' );
                        endwhile;
                    ?>
                </div>
            </div>

        </section>
        <?php
    else :
        if ( is_admin() ) :
            ?>
            <h3 class="h4 top-padding-xs bottom-padding-xs">No blog posts were found matching the selected criteria.</h3>
            <?php
        endif;
    endif;