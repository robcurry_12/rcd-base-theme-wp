<?php
/**
 * Partial used apart of the Blog posts block
 */
?>

<article class="single-post card">
    <div class="content-container">
        
        <div class="image-container">
            <?php 
                if ( get_the_post_thumbnail() ) :
                    the_post_thumbnail( );
                else :
                    echo wp_get_attachment_image( get_field( 'placeholder_image', 'option' ), 'baseball-card', false, ['class' => 'placeholder-image' ] );
                endif;

                the_title( '<p class="card-headline">', '</p>' );
            ?>
        </div>
        <div class="card-content">
            
            <p><?php echo rcd_excerpt( 20 ); ?></p>

        </div>

        <?php 
            $cta = [
                'link' => array(
                    'url' => get_the_permalink(),
                    'target' => '',
                    'title' => 'Read More',
                ),
                'cta_color' => 'brand-color-2',
                'cta_type' => 'btn-solid'
            ];
            
            rcd_call_to_action_partial( $cta );
        ?>

    </div>
</article>