<?php 
    $header_layout  = get_field( 'header_layout', 'option' ); 
    $header_content = get_field( 'header_content', 'option' );
?>

<div class="container <?php echo $header_layout; ?>">

    <div>
        <?php if ( $header_layout != 'one-column-split-nav' ) : ?>
            <a class="site-logo" href="<?php echo site_url(); ?>" aria-label="Click to return to Homepage">
                <?php $site_logo = get_field( 'header_logo', 'option' ); 
                if( $site_logo ) : ?>
                    <?php echo wp_get_attachment_image( $site_logo, 'header-logo', [ 'class' => 'header-logo' ] ); ?>
                <?php else : ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-placeholder.png" alt="Placeholder logo" />
                <?php endif; ?>
            </a>
            <div>
                <div class="main-menu-navigation-container">
                
                    <nav class="navbar main-menu-navigation <?php echo $header_layout; ?>">
                        <?php 
                        wp_nav_menu( 
                            array( 
                                    'theme_location' => 'main-menu', 
                                    'menu_class' => 'main-menu', 
                                    'container_class' => 'collapse simple-navbar',
                                    'container_id' => 'simple-navigation',
                                    'walker' =>  new submenu_dropdown(),
                                ) 
                        ); ?>
                    </nav>
                    <div class="mobile-only">

                        <?php
                            if ( $header_content == 'cta-button' ) :
                                ?>

                                <div class="cta-buttons">

                                    <?php
                                    while( have_rows( 'header_ctas', 'option' ) ) : the_row();
                                        $cta_button_link          = get_sub_field( 'header_cta_link' );
                                        $cta_button_color         = get_sub_field( 'header_cta_color' );
                                        $cta_button_button_type   = get_sub_field( 'header_cta_button_type' );
                                        $cta_button_icon          = get_sub_field( 'header_cta_icon' );
                                        $cta_button_icon_position = get_sub_field( 'header_cta_icon_position' );
                                        
                                        ?>
                                        <a tabindex="0" href="<?php echo esc_url( $cta_button_link['url'] ); ?>" target="<?php echo esc_attr( $cta_button_link['target'] ); ?>" class="cta-button btn <?php echo esc_attr( $cta_button_color) . ' ' . esc_attr( $cta_button_button_type ) . ' ' . esc_attr( $cta_button_icon_position ); ?>" >
                                            <?php echo $cta_button_link['title']; ?>
                                            <?php echo $cta_button_icon; ?>
                                        </a>
                                        <?php
                                    endwhile;
                                    ?>

                                </div>
                                <?php
                            endif;
                        ?>

                        <?php if ( have_rows( 'social_media_accounts', 'option' ) ) : ?>
                            <nav role="navigation" aria-label="Social media accounts" class="social-media-nav">
                                <ul>
                                    <?php while( have_rows( 'social_media_accounts', 'option' ) ) : the_row(); ?>

                                        <li>
                                            <a href="<?php echo esc_url( get_sub_field( 'account_url' ) ); ?>" target="_blank" title="<?php echo ( get_field( 'site_name', 'option' ) ) ? get_field( 'site_name', 'option' ) : ''; ?> on <?php echo esc_attr( get_sub_field( 'account' ) ); ?>">
                                                <?php echo ( get_sub_field( 'font_awesome_code' ) ); ?>
                                            </a>
                                        </li>

                                    <?php endwhile; ?>
                                </ul>
                            </nav>
                        <?php endif; ?>

                    </div>

                </div><!-- .main-menu-navigation-container -->

                <?php
                    if ( $header_content == 'cta-button' ) :
                        ?>

                        <div class="cta-buttons">

                            <?php
                            while( have_rows( 'header_ctas', 'option' ) ) : the_row();
                                $cta_button_link          = get_sub_field( 'header_cta_link' );
                                $cta_button_color         = get_sub_field( 'header_cta_color' );
                                $cta_button_button_type   = get_sub_field( 'header_cta_button_type' );
                                $cta_button_icon          = get_sub_field( 'header_cta_icon' );
                                $cta_button_icon_position = get_sub_field( 'header_cta_icon_position' );
                                
                                ?>
                                <a tabindex="0" href="<?php echo esc_url( $cta_button_link['url'] ); ?>" target="<?php echo esc_attr( $cta_button_link['target'] ); ?>" class="cta-button btn <?php echo esc_attr( $cta_button_color) . ' ' . esc_attr( $cta_button_button_type ) . ' ' . esc_attr( $cta_button_icon_position ); ?>" >
                                    <?php echo $cta_button_link['title']; ?>
                                    <?php echo $cta_button_icon; ?>
                                </a>
                                <?php
                            endwhile;
                            ?>

                        </div>
                        <?php
                    endif;
                ?>
            </div>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#simple-navigation" aria-controls="simple-navigation" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="fa-sharp fa-regular fa-bars"></i></span>
            </button>

        <?php endif; ?>
    </div>
        

</div>
