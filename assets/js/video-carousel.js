(function( $ ) {

    $('.video-carousel-container').slick({
        infinite: true,
        loop: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        centerMode: true,
        arrows: false,
        dots: true,
        lazyLoad: 'progressive',
        cssEase: 'ease-in-out',
        responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
      });


}) ( jQuery );