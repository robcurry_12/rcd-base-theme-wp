(function($) {

    $('.accordion .accordion-title button').on( 'click', function(e) {
        e.preventDefault();
        let expanded = ( $(this).attr("aria-expanded") == 'true' );
        $(this).attr('aria-expanded', ! expanded );
        $(this).closest('.accordion').toggleClass('opened');
        $(this).closest('.accordion').find('.accordion-content').slideToggle();

    });
    
}(jQuery) );