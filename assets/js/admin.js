( function( $ ) { 
    $(window).on ( 'load', function() {
        $('body.wp-admin div[data-name="brand_colors"] td[data-name="color"]' ) .each( function( index ) { 
            let color = $(this).find( '.acf-input-wrap input[type="text"]' ).val();
            $(this).find( '.acf-input-wrap' ).css('--brand-color', color );
        });
        $('body.wp-admin').on( 'input', 'div[data-name="brand_colors"] td[data-name="color"] .acf-input-wrap input[type="text"]', function() {
            let color = $(this).val();
            $(this).closest( '.acf-input-wrap' ).css('--brand-color', color );
        });
        var urlParams = new URLSearchParams(window.location.search);      
        if ( urlParams.has('post_type') && urlParams.has('category') ) {
            $("#menu-pages li.wp-first-item").removeClass('current');
            if ( urlParams.get('category') == 'landing-page' ) {
                $('#menu-pages li:has(a[href="edit.php?post_type=page&category=landing-page"])').addClass('current');
            } else if ( urlParams.get('category') == 'website-page' ) {
                $('#menu-pages li:has(a[href="edit.php?post_type=page&category=website-page"])').addClass('current');
            }
        }  
    });

}) ( jQuery );