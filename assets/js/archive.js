( function ( $ ) { 

    $('body.post-type-archive-rcd_work select').on( 'change', function(e) {

        let filterBy = $(this).attr( 'name' );
        let workType = $(this).val();
        if ( workType != '' ) {
            $('.work-container .single-work:not([data-' + filterBy + '*="|' + workType + '|"])').slideUp();
            $('.work-container .single-work[data-' + filterBy + '*="|' + workType + '|"]' ).slideDown();
        } else {
            $('.work-container .single-work').slideDown();
        }

    });

})( jQuery );