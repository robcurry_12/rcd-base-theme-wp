import { Fancybox } from "@fancyapps/ui"; 

(function($) {
	//main();
    let $window                    = $( window );
	let $siteHeader				   = $('header.site-header');
    let $mainMenuNavigation        = $('header.site-header .main-menu-navigation-container');
    let $mainMenuNavigationToggler = $('header.site-header button.navbar-toggler');
	const navigationPosition       = $mainMenuNavigation.position().top;

    Fancybox.bind("[data-fancybox]", {
		
	});
	$(window).on( 'load', function(e) {
		if ( $('.fancybox-pop-up').length ) {
			$('.fancybox-pop-up').each( function( index ) {
				let anchorTag = $(this).attr( 'id' );
				$('a[href="#' + anchorTag + '"]').off( 'click' );
				Fancybox.bind('a[href="#' + anchorTag + '"]', {
	
				});
				
			});
		}
	});

    $mainMenuNavigationToggler.on( 'click', function(e){

		$(this).find('i').toggleClass('fa-bars');
		$(this).find('i').toggleClass('fa-times');
		$('body').toggleClass('opened');
		if ( $('body').hasClass('opened') ) {
			$(this).attr('aria-expanded', 'true');
		} else {
			$(this).attr('aria-expanded', 'false');
		}

    });

    $(document).on( 'click', function(e) {

        if ( $(e.target).is('body') && $(e.target).hasClass('opened') ) {
            $('body').removeClass('opened');
			$('body').css( 'top', '' );
        }

    });

	if ( $(window).width() > 992 ) {
		$(window).on( 'scroll load', function(e) {
			if ( $(window).scrollTop() - 32  >  navigationPosition && $siteHeader.hasClass('transparent') ) {
				$siteHeader.addClass('dark-bg');
			} else {
				$siteHeader.removeClass('dark-bg')
			}
		});
	}

    $mainMenuNavigation.find( 'li.menu-item > span.dropdown-arrow' ).on( 'click', function(e) {

		e.preventDefault();
		let clickedMenuItem     = $(this).closest( 'li.menu-item' );      
		clickedMenuItem.toggleClass( 'opened' );
		clickedMenuItem.siblings('li').removeClass('opened');

    });
	
	$('body').on( 'click', 'a[href^="#"]:not([href="#load-more"])', function(e) {
		e.preventDefault();
		history.pushState({}, "", $(this).attr('href') );
		$('html, body').animate({
			scrollTop: $( $(this).attr('href')).offset().top - $mainMenuNavigation.height()
		}, 2000);
	  });


})( jQuery );