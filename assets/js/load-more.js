(function($, themeVars) {

    $('a.load-more').on ( 'click', function(e) {
        e.preventDefault();
        let clickedLink  = $(this);
        let postType     = $(this).data('post-type');
        let postsPerPage = $(this).data('posts-per-page');
        let pageNum      = $(this).data('page'); 

        clickedLink.addClass("loading");
        $.ajax({
            url: themeVars.ajax.url,
            type: 'POST',
            data: {
                action: 'load_more_posts',
                nonce: themeVars.ajax.nonce,
                postType : postType,
                postsPerPage : postsPerPage,
                page : pageNum,
            },
            dataType: 'json',
            success: function(res) {
                clickedLink.before( res.data.results );
                if ( res.data.hide_load_more ) {
                    clickedLink.remove();
                } else {
                    clickedLink.removeClass('loading');
                }  	
            }
        });
    });

})( jQuery, rcd );