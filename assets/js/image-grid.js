( function( $ ) {
    
    let slidesToShow = ( $('.layout--image-grid.slider').hasClass('four-columns') ) ? 4 : 3; 

    $('.layout--image-grid.slider .images-container').slick({
        infinite: true,
        loop: true,
        slidesToShow: slidesToShow,
        slidesToScroll: 1,
        centerMode: true,
        arrows: false,
        dots: true,
        lazyLoad: 'progressive',
        cssEase: 'ease-in-out',
        responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: slidesToShow,
          }
        },
        {
            breakpoint: 992,
            settings: {
              slidesToShow: 3,
            }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 1,
          }
        }
        ]
      });
      $(window).on( 'load resize', function(e) {
        if ( $(window).width() < 768 ) {
          $('.layout--image-grid.mobile-slider .images-container').slick({
            infinite: true,
            loop: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            centerMode: true,
            arrows: false,
            dots: true,
            lazyLoad: 'progressive',
            cssEase: 'ease-in-out',
            responsive: [
              {
                breakpoint: 575,
                settings: {
                  slidesToShow: 1,
                }
              }
              ]
          });
        } else {
          $('.layout--image-grid.mobile-slider .images-container').slick('unslick');
        }
      });

}) ( jQuery );