(function($) {

    $('.layout--billboard.multiple-images .image-container').slick({
        arrows: false,
        dots: true,
        slidesToShow: 1,
        adaptiveHeight: true,
    });

})( jQuery );