<?php

//***************************
// Theme Setup Functions
//***************************

/* Setting up constants */
define( 'RCD_BASE_DIRECTORY', get_template_directory_uri() );
define( 'RCD_BASE_IMAGES', get_template_directory_uri() . '/assets/images/' );
define( 'RCD_DESKTOP_SIZE', 1600 );
define( 'RCD_MOBILE_SIZE', 575 );

add_filter( 'wpcf7_autop_or_not', '__return_false' );

/* Registering Navigation Menu Locations */
register_nav_menus( array(
    'sitemap' => esc_html__( 'Sitemap',  'rcd' ),
    'mobile' => esc_html__( 'Mobile', 'rcd' ),
    'main-menu' => esc_html__( 'Main Menu', 'rcd'),
) );

//********************************
//	Enqueue stylesheets & scripts
//********************************
function rcd_base_styles_scripts(){

    /* JQuery Scripts */
    wp_enqueue_script( 'jquery' );

    /* Font Awesome Stylesheet */
    wp_enqueue_script( 'font-awesome', 'https://kit.fontawesome.com/94fd48a81c.js', array('jquery'), true);

    /* Slick Slider Stylesheet & Script */
    wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/css/slick.css');
    wp_enqueue_script( 'slick', get_template_directory_uri() . '/assets/js/slick.min.js', null, '1.8.1', true );

    /* AOS Animation Stylesheet & Script */
    //wp_enqueue_style( 'aos', get_template_directory_uri() . '/assets/css/aos.css');
    //wp_enqueue_script( 'aos-js', get_template_directory_uri() . '/assets/js/aos.min.js', null, '2.3.1', true );

    /* Wordpress Theme Stylesheet */
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );

    /* Base Stylesheet */
    //wp_enqueue_style( 'base', get_template_directory_uri() . '/assets/css/base.css', '5.0' );
}
add_action( 'wp_enqueue_scripts', 'rcd_base_styles_scripts' );

//******************************//
//  Specialized Function Files  //
//******************************//

/* Customizing the WP Admin */
include_once(__DIR__ . '/inc/admin.php');

/* Custom Actions & Hooks */
include_once(__DIR__ . '/inc/actions-hooks.php');

/* Block Editor Actions & Functions */
include_once(__DIR__ . '/inc/block-editor.php');

/* Custom Blocks to be added to Block Editor */
include_once(__DIR__ . '/inc/custom-blocks.php');

/* Helper Functions */
include_once(__DIR__ . '/inc/helpers.php');

/* Navigation Functions */
include_once(__DIR__ . '/inc/navigation.php');

/* Custom shortcodes */
include_once(__DIR__ . '/inc/shortcodes.php');

/* Theme Setup Functions */
include_once(__DIR__ . '/inc/theme-setup.php');

/* Custom Shortcodes */
include_once(__DIR__ . '/inc/shortcodes.php');

/* Scripts & Style enqueing */
include_once(__DIR__ . '/inc/scripts-styles.php');

/* Custom Post Types & Taxonomies */
include_once(__DIR__ . '/inc/custom-post-types.php');
include_once(__DIR__ . '/inc/custom-taxonomies.php');
/* User Roles */
include_once(__DIR__ . '/inc/user-roles.php');

$fonts          = get_field( 'fonts', 'option' );
$colors         = get_field( 'colors', 'option' );

//echo '<pre>' . print_r( $fonts, true ) . '</pre>';

function rcd_generate_theme_json() {

    $fonts          = get_field( 'fonts', 'option' );
	$colors         = get_field( 'colors', 'option' );

	$core_blocks    = array( 'core/paragraph', 'core/heading' );
	$font_sizes	    = array( 'xs', 'sm', 'lg', 'xl' );

	$theme_settings = array( 
		"version" => 1,
        "settings" => array(
			"color" => array(
				"custom" => false,
				"customGradient" => false,
				"defaultGradients" => false,
				"defaultPalette" => false
			),
            "typography" => array(
                "fluid" => true,
				"customFontSize" => false,

            ),
			/*"spacing" => array(
				"spacingSizes" => array(
					"size" => calculate_clamp_output( 24, , RCD_MOBILE_SIZE, RCD_DESKTOP_SIZE ),
					"slug" => 
				)
			)*/
            "blocks" => "",
		)	
	);

	/* Font sizes for XS through XL */
	foreach( $font_sizes as $font_size ) :

		$font_size_values = $fonts['font_style_body_' . $font_size ];
		$theme_settings['settings']['typography']['fontSizes'][] = array(
			"slug"  => $font_size,
			"size"  => $font_size_values['size'],
			"name"  => strtoupper( $font_size ),
			"fluid" => array( 
				"min" => $font_size_values['mobile_size'],
				"max" => $font_size_values['size'],
			)
		);
	endforeach;

	$i = 1;
	foreach ( $core_blocks as $core_block ) :
	
		$block = array( 
			$core_block => array(
				"color" => array(
					"background" => true,
					"palette" => array(	
					)
				)
			)
		);

		$j = 1;
		foreach( $colors['brand_colors'] as $color ) :
			$block[$core_block]["color"]["palette"][] = array(
				"slug" => "brand-color-" . $j,
				"color" => "var(--rcd-brand-color-" . $j . ")",
				"name" => "Brand Color " . $j
			);
			$j++;
		endforeach;

		$block[$core_block]["color"]["palette"][] = array(
			"slug" => "heading-color",
			"color" => "var(--rcd-text-color-heading)",
			"name" => "Heading Color"
		);
		$block[$core_block]["color"]["palette"][] = array(
			"slug" => "body-color",
			"color" => "var(--rcd-text-color-body)",
			"name" => "Body Color"
		);
		$block[$core_block]["color"]["palette"][] = array(
			"slug" => "pure-white",
			"color" => "#ffffff",
			"name" => "Pure White"
		);
		$block[$core_block]["color"]["palette"][] = array(
			"slug" => "pure-black",
			"color" => "#000000",
			"name" => "Pure Black"
		);


		if ( $core_block == 'core/heading' ) :
			for( $i = 1; $i < 7; $i++ ) :
				$block[$core_block]['typography']['fontSizes'][] = array(
					"slug"  => 'h' . $i,
					"size"  => $fonts['font_style_h' . $i ]['size'],
					"name"  => 'Heading ' . $i,
					"fluid" => array( 
							"min" => $fonts['font_style_h' . $i]['mobile_size'],
							"max" => $fonts['font_style_h' . $i]['size']
						)
				);

			endfor;
		endif;
		if ( gettype($theme_settings['settings']['blocks'] ) == 'array' ) :
			$theme_settings['settings']['blocks'] += $block;
		else :
			$theme_settings['settings']['blocks'] = $block;
		endif;
	endforeach;

	$json_data = json_encode( $theme_settings, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES );
	file_put_contents( get_stylesheet_directory(  ). '/theme.json', $json_data );
}

add_action( 'acf/save_post', 'rcd_update_theme_json' );
function rcd_update_theme_json( $post_id ) {
	if ( $post_id === 'options' ) :
		rcd_generate_theme_json();
	endif;
}
