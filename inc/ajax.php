<?php
/**
 * 
 * 
 */
function load_more_posts( $query ) {

    check_ajax_referer( 'rcd_ajax', 'nonce' );

    $post_type      = $_POST['postType'];
    $posts_per_page = $_POST['postsPerPage'];
    $page_num       = $_POST['page'];

    $args = array(
        'post_type'      => $post_type,
        'post_status'    => 'publish',
        'posts_per_page' => $posts_per_page,
        'offset'         => ( $posts_per_page * $page_num )
    );

    $posts = get_posts( $args );

    error_log( print_r( $args, true ) );
    error_log( print_r( $posts, true ) );

    if ( is_countable( $posts ) ) :
        ob_start();
        global $post;
        foreach( $posts as $post ) :
            setup_postdata( $post );
            get_template_part( 'template-parts/partials/content-single' );
        endforeach;
        wp_reset_postdata( $post );
        $results = ob_get_clean();
        $hide_load_more = ( wp_count_posts( 'post' )->publish >= $posts_per_page * $page_num ) ? true : false;
        wp_send_json_success( [
            'results'        => $results,
            'hide_load_more' => $hide_load_more
        ] );
    else :
        wp_send_json_success( false );
    endif;

}
add_action('wp_ajax_load_more_posts', 'load_more_posts');
add_action('wp_ajax_nopriv_load_more_posts', 'load_more_posts');