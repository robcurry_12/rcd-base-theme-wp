<?php 
/**
 * Register Custom Post Types
 * and any other custom post type 
 * related functions
 */
function rcd_custom_post_types() {

    /**
     * An array of labels for this post type.
     *
     * @link https://developer.wordpress.org/reference/functions/get_post_type_labels/
     */
    $labels = [
        'name'                     => _x( 'FAQs', 'FAQs general name', 'rcd' ),
        'singular_name'            => _x( 'FAQ', 'FAQ singular name', 'rcd' ),
        'menu_name'                => _x( 'FAQs', 'FAQ menu name', 'rcd' ),
        'add_new'                  => _x( 'Add New', 'FAQ post', 'rcd' ),
        'add_new_item'             => __( 'Add New FAQ', 'rcd' ),
        'edit_item'                => __( 'Edit FAQ', 'rcd' ),
        'new_item'                 => __( 'New FAQ', 'rcd' ),
        'view_item'                => __( 'View FAQ', 'rcd' ),
        'view_items'               => __( 'View FAQs', 'rcd' ),
        'search_items'             => __( 'Search FAQs', 'rcd' ),
        'not_found'                => __( 'No FAQs found.', 'rcd' ),
        'not_found_in_trash'       => __( 'Not found in trash.', 'rcd' ),
        'parent_item_colon'        => __( 'Parent FAQ:', 'rcd' ),
        'all_items'                => __( 'All FAQs', 'rcd' ),
        'archives'                 => __( 'FAQs Archives', 'rcd' ),
        'attributes'               => __( 'Post Attributes', 'rcd' ),
        'insert_into_item'         => __( 'Insert into post', 'rcd' ),
        'uploaded_to_this_item'    => __( 'Uploaded to this post', 'rcd' ),
        'featured_image'           => _x( 'Featured Image', 'FAQ post', 'rcd' ),
        'set_featured_image'       => _x( 'Set featured image', 'FAQ post', 'rcd' ),
        'remove_featured_image'    => _x( 'Remove featured image', 'FAQ post', 'rcd' ),
        'use_featured_image'       => _x( 'Use as featured image', 'FAQ post', 'rcd' ),
        'filter_items_list'        => __( 'Filter FAQs list', 'rcd' ),
        'items_list_navigation'    => __( 'FAQs list navigation', 'rcd' ),
        'items_list'               => __( 'FAQs list', 'rcd' ),
        'item_published'           => __( 'Post published.', 'rcd' ),
        'item_published_privately' => __( 'Post published privately.', 'rcd' ),
        'item_reverted_to_draft'   => __( 'Post reverted to draft.', 'rcd' ),
        'item_scheduled'           => __( 'Post scheduled.', 'rcd' ),
        'item_updated'             => __( 'Post updated.', 'rcd' ),
    ];

    /**
     * Arguments for registering this post type.
     *
     * @link https://developer.wordpress.org/reference/functions/register_post_type/
     */
    $menu_icon = file_get_contents( get_template_directory() . '/dist/images/comment-question-solid.svg' );

    $args = [
        'labels'              => $labels,
        'description'         => 'Frequently Asked Questions',
        'public'              => false,
        'publicly_queryable'  => true,
        'exclude_from_search' => true,
        'show_in_nav_menus'   => false,
        'show_ui'			  => true,
        'menu_icon'           => 'data:image/svg+xml;base64,' . base64_encode( $menu_icon ),
        'menu_position'       => 10,
        'show_in_rest'		  => true,
        'supports'            => [ 'title', 'editor' ],
        'has_archive'         => true,
        'rewrite'             => [
            'slug'      => 'faqs',
        ],
    ];

	register_post_type( 'rcd_faqs', $args );

}

add_action( 'init', 'rcd_custom_post_types', 10 );