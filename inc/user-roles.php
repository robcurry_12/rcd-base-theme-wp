<?php
// Technical Support - Access to everything
// SEO Roles - Access to ALL content types and YOAST no plugins though.

function rcd_add_roles() {
    // Get the 'Administrator' role
    $admin_role = get_role('administrator');
    
    // Clone the 'Administrator' role capabilities
    $client_admin_capabilities = $admin_role->capabilities;

    // Remove capabilities that 'Client Administrators' should not have
    unset( $client_admin_capabilities['activate_plugins'] );
    unset( $client_admin_capabilities['deactivate_plugins'] );
    unset( $client_admin_capabilities['delete_plugins'] );
    unset( $client_admin_capabilities['delete_themes'] );
    
    // Add 'Client Administrator' role with modified capabilities
    add_role( 'client_administrator', 'Client Administrator', $client_admin_capabilities );
}

// Hook the role creation function to the 'init' action
add_action('init', 'rcd_add_roles');


function rcd_hide_menu_items_by_role() {

     // Check if the current user is logged in
     if ( is_user_logged_in() ) {
        // Get the current user's role
        $current_user = wp_get_current_user();
        $user_roles = $current_user->roles;

        // Define the user role for which you want to hide the ACF menu item
        $restricted_role = 'client_administrator'; // Replace with the role you want to restrict

        // Check if the current user has the restricted role
        if ( in_array( $restricted_role, $user_roles ) ) {
            // Remove the ACF menu item
            remove_menu_page( 'edit.php?post_type=acf-field-group' );
            remove_menu_page( 'tools.php' );
            remove_menu_page( 'edit-comments.php' );
            remove_menu_page( 'options-general.php?page=updraftplus' );

        }
    }
}

add_action('admin_menu', 'rcd_hide_menu_items_by_role');
