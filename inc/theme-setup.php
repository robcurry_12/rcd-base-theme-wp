<?php
/**
 * Theme Setup functions
 * 
 * Functions that setup support for
 * various features within the 
 * RCD Apex Wordpress theme
 */

add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );
add_image_size( 'hero-bg-image', 1700, 600, false );
add_image_size( 'floating-card', 400, 275, false );
add_image_size( 'icon-card', 400, 400, false );
add_image_size( 'billboard-image', 500, 300, false );
add_image_size( 'baseball-card', 500, 400, false );


if ( function_exists( 'get_field' ) ) :
    update_option( 'custom_logo', get_field( 'header_logo','option', false ), true );
    update_option( 'site_icon', get_field( 'site_logo','option', false ), true );
endif;

// Removes from admin menu
add_action( 'admin_menu', 'rcd_remove_admin_menus' );
function rcd_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'rcd_remove_comment_support', 100);

function rcd_remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function rcd_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'comments' );
}
add_action( 'wp_before_admin_bar_render', 'rcd_admin_bar_render' );

// Move Yoast to bottom
function rcd_move_yoast() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'rcd_move_yoast');


 ?>
