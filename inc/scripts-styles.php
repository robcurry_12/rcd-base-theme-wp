<?php
/***
 * Scripts & Styles
 * 
 * Enqueing all necessary scripts & styles
 * to properly serve up an RCD Apex theme
 * Wordpress website
 */

 function rcd_enqueue_style_and_scripts() {

    wp_enqueue_style( 'rcd-critical', get_template_directory_uri() . '/dist/css/critical.css', '', 1.0 );
    wp_enqueue_script( 'rcd-critical', get_template_directory_uri() . '/dist/js/critical.js', ['jquery'], 1.0, true );

    wp_enqueue_script( 'accordion-rcd', get_template_directory_uri() . '/dist/js/accordion.js', ['jquery'], 1.0, true );

    if ( is_archive() ) :
      wp_enqueue_style( 'archives', get_template_directory_uri(). '/dist/css/archives.css', '', 1.0 );
      wp_enqueue_script( 'archives', get_template_directory_uri() . '/dist/js/archive.js', ['jquery'], 1.0, true );      
    endif;

    if ( is_single() ) : 

      wp_enqueue_style( 'singles', get_template_directory_uri(). '/dist/css/singles.css', '', 1.0 ); 

    endif;

    if ( is_home() ) :

      wp_enqueue_style( 'blog', get_template_directory_uri() . '/dist/css/blog.css', '', 1.0 );

    endif;

    if ( is_home() || is_post_type_archive( 'rcd_news' ) ) :

      wp_enqueue_script( 'rcd-load-more', get_template_directory_uri() . '/dist/js/load-more.js', ['jquery'], 1.0, true );
      $script_vars = [
        'ajax' => [
          'nonce' => wp_create_nonce( 'rcd_ajax' ),
          'url' => admin_url( 'admin-ajax.php' )
        ],
      ];
      wp_localize_script( 'rcd-load-more', 'rcd', $script_vars );
      wp_enqueue_script( 'rcd-load-more' );

    endif;

    if ( str_contains( get_page_template(), 'templates/legacy/' ) ) :

      wp_enqueue_style( 'legacy', get_template_directory_uri() . '/dist/css/legacy.css', '', 1.0 );
      wp_enqueue_script( 'legacy', get_template_directory_uri() . '/dist/js/legacy.js', ['jquery'], 1.0, true );
      
      if ( str_contains( get_page_template(), 'holidays-2020' ) ) :

        wp_enqueue_script( 'fancybox_4', 'https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js', [], 1.0, true );
        wp_enqueue_style( 'fancybox_4', 'https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css', '', 1.0 );
  
      endif;

    endif;

    if ( str_contains( get_page_template(), 'templates/holidays/' ) ) :
      
      if ( str_contains( get_page_template(), '2023-ai' ) ) :

        wp_enqueue_script( 'holidays-2023', get_template_directory_uri() . '/dist/js/holidays-2023.js', ['jquery'], 1.0, true );
        wp_enqueue_style( 'holidays-2023', get_template_directory_uri() . '/dist/css/holidays.css', '', 1.0 );
        wp_enqueue_style( 'holidays-font', '//fonts.googleapis.com/css2?family=Caveat+Brush&display=swap', '', 1.0);
  
      endif;

    endif;


    if ( basename( get_page_template() ) === 'page-landing.php' ) :

      wp_enqueue_style( 'landing-page', get_template_directory_uri() . '/dist/css/landing.css', '', 1.0 );


    endif;

    global $post;
    if ( have_rows( 'pop_ups', $post->ID ) ) :
      wp_enqueue_script( 'fancybox_4', 'https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js', [], 1.0, true );
      wp_enqueue_style( 'fancybox_4', 'https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css', '', 1.0 );  
    endif;

 }
 add_action( 'wp_enqueue_scripts', 'rcd_enqueue_style_and_scripts' );