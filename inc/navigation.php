<?php 
/****************************/
/*      Menu Walkers        */
/****************************/
class submenu_dropdown extends Walker_Nav_Menu {
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<span class='dropdown-arrow'><i class='fa-solid fa-sort-down'></i></span><div tabindex='1' class='sub-menu-container'><ul class='sub-menu'>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div>\n";
    }
}
?>