<?php
/**
 * Functions all pertaining to the functionality
 * and experience of the Gutenberg block editor
 */

/**
 * Add block styles/scripts for the block editor.
 */
function rcd_gutenberg_scripts() {
	wp_enqueue_script( 'rcd_block-editor', get_template_directory_uri() . '/dist/js/block-editor.js', [ 'jquery' ], '', true );
	wp_enqueue_style( 'rcd_block-editor', get_template_directory_uri() . '/dist/css/block-editor.css' );

	wp_enqueue_script( 'rcd_block-restrictor', get_template_directory_uri() . '/dist/js/block-restrictor.js', [ 'jquery', 'wp-blocks', 'wp-editor' ], 1.0, true );
}
add_action( 'enqueue_block_editor_assets', 'rcd_gutenberg_scripts' );



/**
 * Declaring all of the allowed blocks in the Gutenberg block editor for Apex
 *
 * @param array  $allowed_blocks The current allowed blocks.
 * @param object $post The post.
 */
function rcd_allowed_block_types( $allowed_blocks, $post ) {

	$allowed_blocks = array(
		'core/heading',
		'core/paragraph',
		'core/list',
		'core/image',
		'core/buttons',
		'core/button',
		'acf/hero',
		'acf/classic-editor',
		'acf/icon-cards',
		'acf/baseball-cards',
		'acf/billboard',
		'acf/image-grid',
		'acf/image-masonry',
		'acf/media-masonry',
		'acf/two-columns-text',
		'acf/video',
		'acf/video-grid',
		'acf/video-carousel',
		'acf/instagram-feed',
		'acf/instagram-posts',
		'acf/social-media-icons',
		'acf/mini-billboards',
		'acf/statistics',
		'acf/image',
		'acf/contact-us-two-columns',
	);

	if ( post_type_exists( 'rcd_faqs' ) ) :
		$allowed_blocks[] = 'acf/faqs';
	endif;


	if ( is_plugin_active( 'instagram-feed/instagram-feed.php' ) ) :
		$allowed_blocks[] = 'acf/instagram-feed';
	endif;
	

	return apply_filters( 'rcd_allowed_block_types', $allowed_blocks, $post );
}
add_filter( 'allowed_block_types_all', 'rcd_allowed_block_types', 10, 2 );

add_filter( 'block_categories_all' , function( $categories ) {

    // Adding a new category.
	array_unshift( $categories, array(
		'slug'  => 'apex-content-blocks',
		'title' => 'RCD Apex Content Blocks'
	));

	return $categories;
} );

/* Disabling Block Editor for certain Custom Post Types */
add_filter( 'use_block_editor_for_post_type', 'rcd_disable_gutenberg', 10, 2 );
function rcd_disable_gutenberg( $current_status, $post_type ) {

    if ( $post_type === 'rcd_faqs' ) :
		return false;
	else :
    	return $current_status;
	endif;

}

/* Disabling Block editor for certain Post IDs */
function rcd_disable_block_editor_for_page_ids( $use_block_editor, $post ) {

    $excluded_ids = array( get_option( 'page_for_posts' ) );
    if ( in_array( $post->ID, $excluded_ids ) ) {
        return false;
    }
    return $use_block_editor;
}
add_filter( 'use_block_editor_for_post', 'rcd_disable_block_editor_for_page_ids', 10, 2 );