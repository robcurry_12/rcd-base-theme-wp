<?php
/**
 * File where all of the custom blocks are added. 
 * Blocks created here must be added to the allowed blocks 
 * in the block-editor.php file within this same folder.
 */
    /**
 * Register ACF Blocks
 */
function rcd_acf_init_block_types() {

	// Check function exists.
	if( function_exists('acf_register_block_type') ) {

		// register hero block.
		acf_register_block_type(
			array(
				'name'            => 'hero',
				'title'           => __( 'Hero' ),
				'description'     => __( 'Flexible style Hero block.' ),
				'render_template' => locate_template( 'template-parts/blocks/hero/hero.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/hero.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => 'laptop',
				'align'           => 'full',
				'keywords'        => array( 'hero', 'above', 'above-fold', 'video', 'image' ),
			)
		);

		// register classic editor block.
		acf_register_block_type(
			array(
				'name'            => 'classic-editor',
				'title'           => __( 'Classic Editor' ),
				'description'     => __( 'Wordpress Classic Editor block.' ),
				'render_template' => locate_template( 'template-parts/blocks/classic-editor/classic-editor.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/classic-editor.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/wordpress.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'classic-editor', 'text', 'heading' ),
			)
		);

		//register icon card block
		acf_register_block_type(
			array(
				'name'            => 'icon-cards',
				'title'           => __( 'Icon Cards' ),
				'description'     => __( 'Icon cards block.' ),
				'render_template' => locate_template( 'template-parts/blocks/icon-cards/icon-cards.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/icon-cards.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/icons-solid.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'icon-cards', 'cards', 'call to action', 'image' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'billboard',
				'title'           => __( 'Billboard' ),
				'description'     => __( 'Image side by side with text content' ),
				'render_template' => locate_template( 'template-parts/blocks/billboard/billboard.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/billboard.css',
				'enqueue_script'  => get_template_directory_uri() . '/dist/js/billboard.js',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => 'align-left',
				'align'           => 'full',
				'keywords'        => array( 'billboard', 'text', 'call to action', 'image' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'mini-billboards',
				'title'           => __( 'Mini Billboards' ),
				'description'     => __( 'Mini version of the billboard block' ),
				'render_template' => locate_template( 'template-parts/blocks/mini-billboards/mini-billboards.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/mini-billboards.css',
				'enqueue_script'  => get_template_directory_uri() . '/dist/js/billboard.js',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => 'align-left',
				'align'           => 'full',
				'keywords'        => array( 'billboard', 'text', 'call to action', 'image', 'mini', 'repeater' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'baseball-cards',
				'title'           => __( 'Baseball Cards' ),
				'description'     => __( 'Card block with title in bottom left corner of image' ),
				'render_template' => locate_template( 'template-parts/blocks/baseball-cards/baseball-cards.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/baseball-cards.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/baseball-card.png' ),
				'align'           => 'full',
				'keywords'        => array( 'baseball-cards', 'cards', 'call to action', 'image' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'image',
				'title'           => __( 'Image (RCD Apex)' ),
				'description'     => __( 'Block of a single image spanning the full width of a container. Different from core image block that does not have Block Settings fields' ),
				'render_template' => locate_template( 'template-parts/blocks/image/image.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/image.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/image-solid.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'image' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'image-grid',
				'title'           => __( 'Image Grid' ),
				'description'     => __( 'Block of images laid out in a grid' ),
				'render_template' => locate_template( 'template-parts/blocks/image-grid/image-grid.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/image-grid.css',
				'enqueue_assets' => function(){
					wp_enqueue_style( 'block-image-grid', get_template_directory_uri() . '/dist/css/3-content-blocks/image-grid.css' );
					wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/css/slick.css');
					wp_enqueue_script( 'slick', get_template_directory_uri() . '/dist/js/slick.min.js', null, '1.8.1', true );
					wp_enqueue_script( 'block-image-grid', get_template_directory_uri() . '/dist/js/image-grid.js', array('jquery'), '', true );
				},
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/grid-regular.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'image', 'grid', 'images', 'linked' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'image-masonry',
				'title'           => __( 'Image Masonry' ),
				'description'     => __( 'Block of images laid out in a masonry' ),
				'render_template' => locate_template( 'template-parts/blocks/image-masonry/image-masonry.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/image-masonry.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/cubes-sharp-solid.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'images', 'image', 'masonry' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'media-masonry',
				'title'           => __( 'Media Masonry' ),
				'description'     => __( 'Block of video and images laid out in a masonry' ),
				'render_template' => locate_template( 'template-parts/blocks/media-masonry/media-masonry.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/media-masonry.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/cubes-sharp-solid.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'images', 'videos', 'masonry' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'two-columns-text',
				'title'           => __( 'Two Columns of Text' ),
				'description'     => __( 'Two column block of text based content' ),
				'render_template' => locate_template( 'template-parts/blocks/two-columns-text/two-columns-text.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/two-columns-text.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/line-columns-solid.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'columns', 'text', 'variable width' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'video',
				'title'           => __( 'Video' ),
				'description'     => __( 'Single Video' ),
				'render_template' => locate_template( 'template-parts/blocks/video/video.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/video.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/video-solid.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'video', 'grid' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'video-grid',
				'title'           => __( 'Video Grid' ),
				'description'     => __( 'Grid of videos of either uniform or varying aspect ratios' ),
				'render_template' => locate_template( 'template-parts/blocks/video-grid/video-grid.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/video-grid.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/film-solid.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'video', 'grid' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'video-carousel',
				'title'           => __( 'Video Carousel' ),
				'description'     => __( 'Carousel of videos that expand the full width of the screen of either uniform or varying aspect ratios' ),
				'render_template' => locate_template( 'template-parts/blocks/video-carousel/video-carousel.php' ),
				'enqueue_assets' => function(){
					wp_enqueue_style( 'block-video-carousel', get_template_directory_uri() . '/dist/css/3-content-blocks/video-carousel.css' );
					wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/css/slick.css');
					wp_enqueue_script( 'slick', get_template_directory_uri() . '/dist/js/slick.min.js', null, '1.8.1', true );
					wp_enqueue_script( 'block-video-carousel', get_template_directory_uri() . '/dist/js/video-carousel.js', array('jquery'), '', true );
				},
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/films-solid.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'video', 'carousel' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'blog-posts',
				'title'           => __( 'Blog Posts' ),
				'description'     => __( 'Block that displays Blog Posts in a card style block' ),
				'render_template' => locate_template( 'template-parts/blocks/blog-posts/blog-posts.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/blog-posts.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/newspaper-solid.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'blog posts', 'post' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'social-media-icons',
				'title'           => __( 'Social Media Icons' ),
				'description'     => __( 'Block that displays links to social media accounts with icons' ),
				'render_template' => locate_template( 'template-parts/blocks/social-media-icons/social-media-icons.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/social-media-icons.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/hashtag-solid.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'social media', 'icons' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'statistics',
				'title'           => __( 'Statistics' ),
				'description'     => __( 'Block that displays statistics in a 2-across grid' ),
				'render_template' => locate_template( 'template-parts/blocks/statistics/statistics.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/statistics.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/chart-mixed-solid.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'statistics', 'text' ),
			)
		);

		acf_register_block_type(
			array(
				'name'            => 'instagram-posts',
				'title'           => __( 'Instagram Posts' ),
				'description'     => __( 'Instagram posts embedded via embed codes' ),
				'render_template' => locate_template( 'template-parts/blocks/instagram-posts/instagram-posts.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/instagram-posts.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/instagram.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'instagram', 'embed', 'instagram posts' ),
			)
		);
		
		acf_register_block_type(
			array(
				'name'            => 'contact-us-two-columns',
				'title'           => __( 'Contact Two Columns' ),
				'description'     => __( 'Two column block that takes a team member as a point of contact and a form.' ),
				'render_template' => locate_template( 'template-parts/blocks/contact-us/contact-us.php' ),
				'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/contact-us.css',
				'supports'        => array(
					'align'           => array( 'full' ),
					'jsx'             => true,
					'customClassName' => true,
				),
				'category'        => 'apex-content-blocks',
				'icon'            => file_get_contents( get_template_directory() . '/dist/images/address-book-regular.svg' ),
				'align'           => 'full',
				'keywords'        => array( 'contact us', 'contact', 'two columns' ),
			)
		);
		

		if ( post_type_exists( 'rcd_faqs' ) ) :

			acf_register_block_type(
				array(
					'name'            => 'faqs',
					'title'           => __( 'FAQs' ),
					'description'     => __( 'Accordion style block displaying FAQs' ),
					'render_template' => locate_template( 'template-parts/blocks/faqs/faqs.php' ),
					'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/faqs.css',
					'enqueue_script'  => get_template_directory_uri() . '/dist/js/accordion.js',
					'supports'        => array(
						'align'           => array( 'full' ),
						'jsx'             => true,
						'customClassName' => true,
					),
					'category'        => 'apex-content-blocks',
					'icon'            => file_get_contents( get_template_directory() . '/dist/images/comment-question-solid.svg' ),
					'align'           => 'full',
					'keywords'        => array( 'faqs', 'custom post type', 'accordion' ),
				)
			);	
		endif;


		if ( is_plugin_active( 'instagram-feed/instagram-feed.php' ) ) :

			acf_register_block_type(
				array(
					'name'            => 'instagram-feed',
					'title'           => __( 'Instagram Feed' ),
					'description'     => __( 'Instagram feed configured using the Smash Balloon plugin' ),
					'render_template' => locate_template( 'template-parts/blocks/instagram-feed/instagram-feed.php' ),
					'enqueue_style'   => get_template_directory_uri() . '/dist/css/3-content-blocks/instagram-feed.css',
					'supports'        => array(
						'align'           => array( 'full' ),
						'jsx'             => true,
						'customClassName' => true,
					),
					'category'        => 'apex-content-blocks',
					'icon'            => file_get_contents( get_template_directory() . '/dist/images/instagram.svg' ),
					'align'           => 'full',
					'keywords'        => array( 'instagram', 'smash balloon' ),
				)
			);	


		endif;
    }
}
add_action('init', 'rcd_acf_init_block_types', 99);

?>