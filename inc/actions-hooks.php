<?php  
/**
 * Actions & Hooks
 * 
 * List of actions and hooks utilized throughout
 * the Apex Wordpress theme
 */

add_action( 'rcd_site_head', 'rcd_head_before' );
function rcd_head_before() {

    $head_scripts = get_field( 'head_scripts', 'option' );
    printf( '%s', $head_scripts );

}
add_action( 'rcd_body_before', 'rcd_bypass_block' );
function rcd_bypass_block() {

    printf( '%s', '<a href="#site-content" class="bypass-block" tabindex="0">Skip to main content</a>' );

}
add_action( 'rcd_body_before', 'rcd_body_open' );
function rcd_body_open() {

    $body_open_scripts = get_field( 'body_open_scripts', 'option' );
    printf( '%s', $body_open_scripts );
    
}

add_action( 'rcd_body_after', 'rcd_body_close' );
function rcd_body_close() {

    $body_close_scripts = get_field( 'body_close_scripts', 'option' );
    printf( '%s', $body_close_scripts );
    
    global $post;
    if ( have_rows( 'pop_ups', $post->ID ) ) :
        $i = 0;
        while ( have_rows( 'pop_ups', $post->ID ) ) :
            the_row();
            $i++;
            ?>
            <div class="fancybox-pop-up pop-up-num-<?php echo $i; ?>" id="<?php echo esc_attr( get_sub_field( 'id' ) ); ?>" style="display:none;">
                <div class="content-container">
                    <?php echo apply_filters( 'wpcf7_form_elements', apply_filters( 'the_content', get_sub_field( 'content' ) ) ); ?>
                </div>
            </div>
            <?php
        endwhile;
    endif;

}

add_action( 'rcd_site_head', 'rcd_google_fonts' );
add_action( 'admin_head', 'rcd_google_fonts' );
add_action( 'login_head', 'rcd_google_fonts' );
function rcd_google_fonts() {

    $google_fonts = get_field( 'google_fonts', 'option' );
    ?>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <?php
    printf( '%s', $google_fonts );

}

add_action( 'rcd_site_head', 'rcd_theme_options' );
add_action( 'admin_head', 'rcd_theme_options' );
add_action( 'login_head', 'rcd_theme_options' );
function rcd_theme_options() {

    $rcd_colors = get_field( 'colors', 'option' );
    $rcd_fonts  = get_field( 'fonts', 'option' );
    ?>
    <style type="text/css" id="rcd-base-dancedance">
        :root {
            <?php
            $i = 1;
            foreach( $rcd_colors['brand_colors'] as $brand_color ) :

                echo '--rcd-brand-color-' . $i . ': ' . esc_attr( $brand_color['color'] ) . ';' . PHP_EOL;
                $i++;

            endforeach;
            $i = 1;

            global $post;
            if ( gettype( $post ) !== 'NULL' ) :
                if ( ! empty( get_field( 'client_brand_color', $post->ID ) ) ) :
                    echo '--rcd-client-brand-color:' . esc_attr( get_field( 'client_brand_color', $post->ID ) ) . ';' . PHP_EOL;
                endif;
            endif;
            ?>

            --rcd-text-color-heading: <?php echo esc_attr( $rcd_colors['text_colors']['heading'] ); ?>;
            --rcd-text-color-body: <?php echo esc_attr( $rcd_colors['text_colors']['body'] ); ?>;
            --rcd-color-off-white-1: #f0f0f0;
            --rcd-color-off-white-2: #f5f5f5;
            --rcd-color-off-black-1: #3c3c3c;
            --rcd-color-off-black-2: #555555;
            --rcd-color-off-black-3: #333333;
            <?php
            $font_families = array();
            for( $i = 1; $i < 7; $i++ ) :
                ?>

                --rcd-font-h<?php echo $i; ?>-size: <?php echo calculate_clamp_output( $rcd_fonts['font_style_h' . $i]['mobile_size'], $rcd_fonts['font_style_h' . $i]['size'], RCD_MOBILE_SIZE, RCD_DESKTOP_SIZE); ?>;
                --rcd-font-h<?php echo $i; ?>-font-family: "<?php echo esc_attr( $rcd_fonts['font_style_h' . $i]['font_family'] ); ?>";
                --rcd-font-h<?php echo $i; ?>-letter-spacing: <?php echo ( ! empty( esc_attr( $rcd_fonts['font_style_h' . $i]['letter_spacing'] ) ) ) ? esc_attr( $rcd_fonts['font_style_h' . $i]['letter_spacing'] ) : 0; ?>;
                --rcd-font-h<?php echo $i; ?>-text-transform: <?php echo esc_attr( substr( $rcd_fonts['font_style_h' . $i]['text_transform'], 15 ) ); ?>;
                --rcd-font-h<?php echo $i; ?>-font-weight: <?php echo esc_attr( $rcd_fonts['font_style_h' . $i]['font_weight'] ); ?>;
                --rcd-font-h<?php echo $i; ?>-line-height: <?php echo esc_attr( $rcd_fonts['font_style_h' . $i]['line_height'] ); ?>;

                <?php
            endfor;?>
            
            --rcd-font-size-base                : <?php echo calculate_clamp_output( $rcd_fonts['font_style_body']['mobile_size'], $rcd_fonts['font_style_body']['size'], RCD_MOBILE_SIZE, RCD_DESKTOP_SIZE); ?>;
            --rcd-font-size-base-letter-spacing : <?php echo esc_attr( $rcd_fonts['font_style_body']['letter_spacing'] ); ?>;
            --rcd-font-size-base-line-height    : <?php echo esc_attr( $rcd_fonts['font_style_body']['line_height'] ); ?>;
            
            --rcd-font-size-xs                  : <?php echo calculate_clamp_output( $rcd_fonts['font_style_body_xs']['mobile_size'], $rcd_fonts['font_style_body_xs']['size'], RCD_MOBILE_SIZE, RCD_DESKTOP_SIZE); ?>;
            --rcd-font-size-xs-letter-spacing   : <?php echo esc_attr( $rcd_fonts['font_style_body_xs']['letter_spacing'] ); ?>;
            --rcd-font-size-xs-line-height      : <?php echo esc_attr( $rcd_fonts['font_style_body_xs']['line_height'] ); ?>;
            
            --rcd-font-size-sm                  : <?php echo calculate_clamp_output( $rcd_fonts['font_style_body_sm']['mobile_size'], $rcd_fonts['font_style_body_sm']['size'], RCD_MOBILE_SIZE, RCD_DESKTOP_SIZE); ?>;
            --rcd-font-size-sm-letter-spacing   : <?php echo esc_attr( $rcd_fonts['font_style_body_sm']['letter_spacing'] ); ?>;
            --rcd-font-size-sm-line-height      : <?php echo esc_attr( $rcd_fonts['font_style_body_sm']['line_height'] ); ?>;
            
            --rcd-font-size-lg                  : <?php echo calculate_clamp_output( $rcd_fonts['font_style_body_lg']['mobile_size'], $rcd_fonts['font_style_body_lg']['size'], RCD_MOBILE_SIZE, RCD_DESKTOP_SIZE); ?>;
            --rcd-font-size-lg-letter-spacing   : <?php echo esc_attr( $rcd_fonts['font_style_body_lg']['letter_spacing'] ); ?>;
            --rcd-font-size-lg-line-height      : <?php echo esc_attr( $rcd_fonts['font_style_body_lg']['line_height'] ); ?>;
           
            --rcd-font-size-xl                  : <?php echo calculate_clamp_output( $rcd_fonts['font_style_body_xl']['mobile_size'], $rcd_fonts['font_style_body_xl']['size'], RCD_MOBILE_SIZE, RCD_DESKTOP_SIZE); ?>;
            --rcd-font-size-xl-letter-spacing   : <?php echo esc_attr( $rcd_fonts['font_style_body_xl']['letter_spacing'] ); ?>;
            --rcd-font-size-xl-line-height      : <?php echo esc_attr( $rcd_fonts['font_style_body_xl']['line_height'] ); ?>;

            --rcd-font-family-base: <?php echo esc_attr( $rcd_fonts['font_style_body']['font_family'] ); ?>;
        }
        <?php
            $i = 1;
            foreach( $rcd_colors['brand_colors'] as $brand_color ) :
                ?>
                .brand-color-<?php echo $i; ?>  {
                    --block-background-color: var( <?php echo '--rcd-brand-color-' . $i;?> );
                }
                <?php
                $i++;
            endforeach;
        ?>
    </style>
    <?php
}

add_action( 'rcd_container_before', 'rcd_container_open' );
function rcd_container_open(){
    $module_width = get_sub_field( 'module_width' );
    if ( 'fixed' === $module_width ) :
        echo '<div class="container">';
    endif;
}

add_action( 'rcd_container_after', 'rcd_container_close' );
function rcd_container_close(){
    $module_width = get_sub_field( 'module_width' );
    if ( 'fixed' === $module_width ) :
        echo '</div>';
    endif;
}
?>
