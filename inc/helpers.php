<?php
/***
 * Helper Functions file
 * 
 * This file contains helper functions that help
 * make computations & comparisons easier and 
 * less lines of code in the middle of templates
 * 
 */

/* Checks if number is odd */
function is_odd($num){
    return $num % 2 != 0;
}

/* Checks if number is even */
function is_even($num){
    return $num % 2 == 0;
}

/* Checks if ACF Group fields are filled. */
function is_group_filled($group){
    if($group) :
        if( !array_filter( $group )) :
            return false;
        else :
            return true;
        endif;
    else :
        return false;
    endif;
}

/* Checks if field has value and displays CSS class in the form of a string  */
function if_set_display_class($value, $true_class, $false_class = null){
    if($value) :
        return $true_class;
    else :
        return $false_class;
    endif;
}

/* Prints out the user entered field values as attributes of the element */
function rcd_block_settings( $block_name, array $classes ) {
    
    $block_id      = get_field( 'block_id' );
    $classes[]     = 'rcd-acf-block';
    $classes[]     = 'layout--' . $block_name;
    $classes[]     = get_field( 'block_classes' );

    
    /** Padding Values **/
    $padding       = get_field( 'block_padding' );
    if ( $padding ) :
        $classes[]     = $padding['top'] . '-top';
        $classes[]     = $padding['bottom'] . '-bottom';
    endif;

    if ( get_field( 'offset_background' ) ) :
        $classes[] = 'offset-background';
    endif;

    if ( get_field( 'white_text' ) ) :
        $classes[] = 'has-white-text';
    endif;

    /** Background color values **/
    $bg_color      = ( get_field( 'block_background_color' ) != 'null' ) ? 'has-bg-color ' . get_field( 'block_background_color' ) : '';
    $classes[]     = $bg_color;

    $element_attributes = '';
    
    /* Adding id attribute */
    if( $block_id ) : 
        $element_attributes .= 'id="' . $block_id . '" ';
    endif;

    if ( get_field( 'block_background_image' ) ) :
        $element_attributes .= 'style="--block-background-image:url(' . get_field( 'block_background_image' ) . ');" ';
        $classes[] = 'has-bg-image';
    endif;
    
    if ( get_field( 'background_parallax' ) ) :
        $classes[] = 'parallax-bg';
    endif;

    /* Adding class attribute */
    $element_attributes .= 'class="' . implode( " ", $classes ) . '" ';

    /* Removing any leading or trailing white space */
    $element_attributes = trim( $element_attributes );

    echo $element_attributes;
    return true;
}

function rcd_block_heading() {

    if ( $has_block_title = get_field( 'block_pre_title' ) || get_field( 'block_title' ) || get_field( 'block_subtitle' ) || ( get_field( 'block_cta' ) && get_field( 'block_cta' )['link'] ) ) :
        ?>
        <div class="rcd-acf-block-title-container <?php echo ( get_field( 'block_anchor_line' ) ) ? 'anchor-line ' : ''; echo 'alignment-' . get_field( 'block_header_alignment' ); ?>">
        <?php
    endif;
    if ( get_field( 'block_pre_title' ) ) :
        ?>
        <p class="rcd-acf-block-pre-title"><?php echo wp_kses_post( get_field( 'block_pre_title' ) ); ?></p>
        <?php
    endif;
    if ( get_field( 'block_title' ) ) :
        ?>
        <h2 class="rcd-acf-block-title"><?php echo wp_kses_post( get_field( 'block_title' ) ); ?></h2>
        <?php
    endif;

    if ( get_field( 'block_subtitle' ) ) :
        ?>
        <h3 class="rcd-acf-block-subtitle"><?php echo wp_kses_post( get_field( 'block_subtitle' ) ); ?></h3>
        <?php
    endif;
    $block_cta = get_field( 'block_cta' );
    if ( $block_cta && $block_cta_link = $block_cta['link'] ) :
        ?>
        <a href="<?php echo esc_url( $block_cta_link['url'] ); ?>" target="<?php echo esc_attr( $block_cta_link['target'] ); ?>" class="rcd-acf-block-cta btn <?php echo esc_attr( $block_cta['cta_type'] ) . ' ' . esc_attr( $block_cta['cta_color'] ); ?>">
            <?php echo esc_html( $block_cta_link['title'] ); ?>
        </a>
        <?php
    endif;
    if ( $has_block_title ) :
        ?>
        </div>
        <?php
    endif;
}
/* Prints out a complete image tag based off an image array from an ACF Image field */
function rcd_display_acf_image($arr, $classes = null){
    $image = '';

    $image .= '<img src="' . $arr['url'] . '"'; 

    if($arr['alt']) : 
        $image .= ' alt="' . $arr['alt'] . '"';
    endif;
    
    if($classes) : 
        $image .= ' class="'. esc_attr( $classes ) . '" ';
    endif;
    
    $image .= '/>';

    echo $image;
}

/* Handles outputting the ACF field group Call to Action partial */
function rcd_call_to_action_partial( $call_to_action_clone_field ) {

    if ( ! is_bool( $call_to_action_clone_field ) ) :
        if ( $cta_clone = $call_to_action_clone_field['link'] ) :
            ?>
            <a href="<?php echo esc_url( $cta_clone['url'] ); ?>" target="<?php echo esc_attr( $cta_clone['target'] ); ?>" class="btn <?php echo esc_attr( $call_to_action_clone_field['cta_type'] ) . ' ' . esc_attr( $call_to_action_clone_field['cta_color'] ); ?>">
                <?php echo esc_html( $cta_clone['title'] ); ?>
            </a>
            <?php
        endif;
    endif;
    
}

/* Limits the number of words inside of an excerpt */
function rcd_excerpt( $limit ) {
    $excerpt = explode( ' ', get_the_excerpt(), $limit );
    if ( count( $excerpt ) >= $limit ) :
      array_pop( $excerpt );
      $excerpt = implode( " ",$excerpt ).'...';
    else :
      $excerpt = implode( " ",$excerpt );
    endif;	
    $excerpt = preg_replace( '`[[^]]*]`','',$excerpt );
    return $excerpt;
}

function calculate_clamp_output ($min_size = 1, $max_size = 1, $min_width = 1, $max_width = 1 ) {
    $min_size = preg_replace("/[^0-9]/", "", $min_size );
    $max_size = preg_replace("/[^0-9]/", "", $max_size );
    // Calculate the slope and intercept of the linear equation y = mx + b.
    $slope = ( intval( $max_size ) - intval( $min_size )) / ( intval( $max_width ) - intval( $min_width ) );
    $rate = 100 * $slope;

    $intercept = -1 * intval( $min_width ) * $slope + intval( $min_size );

    // Build the clamp output string.
    $clamp_output = "clamp({$min_size}px, calc({$intercept}px + {$rate}vw), {$max_size}px)";

    // Return the final clamp output.
    return $clamp_output;
}

/**
 * Output a Get Directions link from Google Maps
 *
 * @param array $address associative array containing address information
 * @return string href value for anchor tag that will bring user to exact location on google maps
 */
function rcd_gmaps_directions( $address ) {
	$link = 'https://maps.google.com/?q=';
	return $link . str_replace( '<br />', '', $address );
}

/**
 * Add iFrame to allowed wp_kses_post tags
 *
 * @param array  $tags Allowed tags, attributes, and/or entities.
 * @param string $context Context to judge allowed tags by. Allowed values are 'post'.
 *
 * @return array
 */
function rcd_custom_wpkses_post_tags( $tags, $context ) {

	if ( 'post' === $context ) {
		$tags['iframe'] = array(
			'src'             => true,
			'height'          => true,
			'width'           => true,
			'frameborder'     => true,
			'allowfullscreen' => true,
		);
	}

	return $tags;
}

add_filter( 'wp_kses_allowed_html', 'rcd_custom_wpkses_post_tags', 10, 2 );

add_filter('wpcf7_form_elements', 'rcd_remove_cf7_wrappers', 10, 1);

function rcd_remove_cf7_wrappers( $content ) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content, -1);

    return $content;
}
