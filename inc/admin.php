<?php
/* Set Login logo to go to homepage */
add_filter( 'login_headerurl', 'rcd_custom_login_url' );
function rcd_custom_login_url( $url ) {
    return site_url( '/' );
}
/* Custom Logo on Wordpress Admin screen */
function rcd_custom_login_logo() {  ?>
    <style type="text/css">
        body.login {
            background-color: var(--rcd-brand-color-3);
        }
        body.login div#login h1 a {
            background-image: url('<?php echo wp_get_attachment_image_src( get_field( 'header_logo', 'option' ), 'header-logo' )[0]; ?>');
            background-size: contain;
            width: 100%;
        }
        body.login #wp-submit {
            background-color: var(--rcd-brand-color-1);
            border: 2px solid var(--rcd-brand-color-1);
            font-size: 16px;
            padding: 10px 16px;
            line-height: 1;
            font-weight: bold;
            text-transform: uppercase;
        }
        body.login #loginform {
            border-radius: 8px;
            box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        }
        body.login #wp-submit:hover,
        body.login #wp-submit:focus {
            background-color: #fff;
            color: var(--rcd-brand-color-1);
        }
        body.login div#login p > a {
            color: #fff;
        } 
        body.login div#login p > a:hover,
        body.login div#login p > a:focus {
            text-decoration: underline;
            color: #fff;
        }
    </style> <?php
}
add_action( 'login_enqueue_scripts', 'rcd_custom_login_logo' );

/* Adding custom styling to Wordpress Admin */
function admin_style() {

	wp_enqueue_style( 'rcd_admin-styles', get_template_directory_uri() . '/dist/css/admin.css' );
    wp_enqueue_script( 'font-awesome', 'https://kit.fontawesome.com/3d57c447c6.js', array('jquery'), true);
    wp_enqueue_script( 'rcd_admin_scripts', get_template_directory_uri() . '/dist/js/admin.js', array('jquery') );

}
add_action('admin_enqueue_scripts', 'admin_style');


/* Creating Site settings options page */
if( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page( array(
		'page_title' 	=> 'Site Settings',
		'menu_title'	=> 'Site Settings',
		'menu_slug' 	=> 'site-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

/* Limit max menu depth in admin panel to 2 */
function q242068_limit_depth( $hook ) {
	if ( $hook != 'nav-menus.php' ) return;
  
	// override default value right after 'nav-menu' JS
	wp_add_inline_script( 'nav-menu', 'wpNavMenu.options.globalMaxDepth = 2;', 'after' );
  }
  add_action( 'admin_enqueue_scripts', 'q242068_limit_depth' );

function rcd_acf_load_cta_color_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();

	$colors = get_field( 'colors', 'option' );
    // if has rows
    if( is_array( $colors ) && is_array( $colors['brand_colors'] ) && is_countable( $colors['brand_colors']) ) {
        
        $i = 1;
        foreach( $colors['brand_colors'] as $color ) {

            $field['choices'][ 'brand-color-' . $i ] = 'Brand Color ' . $i;
			$style = '<style type="text/css">
				.acf-field[data-name*="cta_color"] .acf-input ul li input[value="brand-color-' . $i . '"]{
					background-color: var(--rcd-brand-color-' . $i . ');
				}
			</style>';
			if( ! headers_sent() ) :
			    sprintf( '%s', $style );
            endif;
			$i++;

        }
        
    }

    // return the field
    return $field;
    
}

add_filter('acf/load_field/key=field_64236ba69b86a', 'rcd_acf_load_cta_color_field_choices');

function rcd_acf_load_block_bg_color_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();
    $field['choices']['null'] = 'Transparent';

	$colors = get_field( 'colors', 'option' );
    // if has rows
    if( is_countable( $colors['brand_colors']) ) {
        
        $style = '<style type="text/css">';
        $i = 1;
        foreach( $colors['brand_colors'] as $color ) {

            $field['choices'][ 'brand-color-' . $i ] = 'Brand Color ' . $i;
			$style .= '.acf-field[data-name*="background_color"] .acf-input ul li input[value="brand-color-' . $i . '"]{
					background-color: var(--rcd-brand-color-' . $i . ');
				}';
			$i++;

        }
    }
    global $post;
    if ( isset( $post ) ) :
        if ( ! empty( get_field( 'client_brand_color', $post->ID ) ) ) :
            $field['choices']['client-brand-color'] = 'Client Brand Color';
            $style .= '.acf-field[data-name*="background_color"] .acf-input ul li input[value="client-brand-color"]{
                background-color: var(--rcd-client-brand-color);
            }';
        endif;
    endif;

    $style .= '</style>';

    $field['choices']['off-white-1'] = 'Off White 1';
    $field['choices']['off-white-2'] = 'Off White 2';
    $field['choices']['off-black-1'] = 'Off Black 1';
    $field['choices']['off-black-2'] = 'Off Black 2';
    $field['choices']['off-black-3'] = 'Off Black 3';

    // return the field
    return $field;
    
}
add_filter('acf/load_field/key=field_6463bd337c005', 'rcd_acf_load_block_bg_color_field_choices');


