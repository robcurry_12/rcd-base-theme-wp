</main> <!-- site-content -->

    <?php
        if ( is_group_filled( get_field( 'pre_footer', 'option' ) ) ) : ?>
        <section class="pre-footer rcd-acf-block has-bg-color brand-color-2 full-padding-top full-padding-bottom">
            <div class="container">
                <?php $pre_footer = get_field( 'pre_footer', 'option' ); ?> 
                <div class="rcd-acf-block-title-container anchor-line alignment-left">
                    <?php if ( $pre_footer['pre_title'] ) : ?>
                        <p class="rcd-acf-block-pre-title"><?php echo wp_kses_post( $pre_footer['pre_title'] ); ?></p>
                        <?php
                    endif;
                    if ( $pre_footer['title'] ) :
                        ?>
                        <h2 class="rcd-acf-block-title"><?php echo wp_kses_post( $pre_footer['title'] ); ?></h2>
                        <?php
                    endif;
                    if ( $pf_cta = $pre_footer['call_to_action'] ) :
                        ?>
                        <a href="<?php echo esc_url( $pf_cta['url'] ); ?>" target="<?php echo esc_attr( $pf_cta['target'] ); ?>" class="btn btn-outline brand-color-2">
                            <?php echo wp_kses_post( $pf_cta['title'] ); ?>
                        </a>
                        <?php
                    endif;
                    ?>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <footer class="site-footer">
        <div class="container">

            <?php if ( have_rows( 'locations', 'option' ) ) : ?>

                <div class="locations-container">
                    <label>Locations</label>

                    <?php while( have_rows( 'locations', 'option' ) ) : the_row(); ?>

                        <div class="single-location">

                            <?php if ( get_sub_field( 'title' ) ) : ?>
                                <strong><?php the_sub_field( 'title' ); ?></strong>
                            <?php endif; ?>

                            <?php if ( $address = get_sub_field( 'address' ) ) : ?>
                                <address>
                                    <?php if ( $address['street_address_1'] ) : ?>
                                        <span><?php echo $address['street_address_1']; ?></span>
                                    <?php endif; ?>
                                    <?php if ( $address['street_address_2'] ) : ?>
                                        <span><?php echo $address['street_address_2']; ?></span>
                                    <?php endif; ?>
                                    <?php if ( $address['city'] || $address['state'] || $address['zipcode'] ) : ?>
                                        <span>
                                            <?php echo ( $address['city'] ) ?: $address['city']; ?>, <?php echo ( $address['state'] ) ?: $address['state']; ?> <?php echo ( $address['zipcode'] ) ?: $address['zipcode']; ?>
                                        </span>
                                    <?php endif; ?>
                                </address>
                            <?php endif; ?>

                            <?php if ( $email = get_sub_field( 'email_address' ) ) : ?>
                                <a href="mail:<?php echo $email; ?>"><i class="fa-solid fa-envelope"></i> Email Us</a>
                            <?php endif; ?>

                            <?php if ( $phone_number = get_sub_field( 'phone_number' ) ) : ?>
                                <a href="tel:<?php echo esc_attr( $phone_number ); ?>">
                                    <i class="fa-solid fa-phone"></i><?php echo ( get_sub_field( 'vanity_phone_number' ) ) ? the_sub_field( 'vanity_phone_number' ) : trim( $phone_number ); ?>
                                </a>
                            <?php endif; ?>

                        </div><!--.single-location-->

                    <?php endwhile; ?>            

                </div> <!--.locations-container -->

            <?php endif; ?>

            <?php if ( $footer_biz_logos = get_field( 'footer_business_logos', 'option' ) ) : ?>

                <div class="footer-business-logos-container">

                    <?php
                        if ( is_countable( $footer_biz_logos ) ) :
                            foreach ( $footer_biz_logos as $logo ) :
                                ?>
                                <div class="single-logo">
                                    <span class="caption"><?php echo wp_get_attachment_caption( $logo ); ?></span>
                                    <?php echo wp_get_attachment_image( $logo, 'full', [ 'class' => 'footer-business-logo' ] ); ?>
                                </div>
                                <?php
                            endforeach;
                        endif;
                    ?>

                </div>

            <?php endif; ?>

            <?php
                if ( have_rows( 'footer_navigation', 'option' ) ) :
                    ?>
                    <div class="footer-navigation-container">
                        <nav role="navigation">
                            <ul class="footer-nav">
                            <?php if ( have_rows( 'footer_navigation', 'option' ) ) : ?>
                                <ul class="footer-nav">
                                    <?php while ( have_rows( 'footer_navigation', 'option' ) ) : the_row(); ?>
                                        <li>
                                            <?php $link = get_sub_field( 'nav_link' ); ?>
                                            <a href="<?php echo esc_url( $link['url'] ); ?>" target="<?php echo esc_attr( $link['target'] ); ?>" aria-label="<?php echo esc_attr( $link['title'] ); ?>">
                                                <?php echo esc_html( $link['title'] ); ?>
                                            </a>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?>
                        </nav>
                    </div>
                    <?php
                endif;
            ?>
            
            <div class="social-footer-nav-container">

                <?php if ( have_rows( 'social_media_accounts', 'option' ) ) : ?>
                    <nav role="navigation" aria-label="Social media accounts" class="social-media-nav">
                        <ul>
                            <?php while( have_rows( 'social_media_accounts', 'option' ) ) : the_row(); ?>

                                <li>
                                    <a href="<?php echo esc_url( get_sub_field( 'account_url' ) ); ?>" target="_blank" title="<?php echo ( get_field( 'site_name', 'option' ) ) ? get_field( 'site_name', 'option' ) : ''; ?> on <?php echo esc_attr( get_sub_field( 'account' ) ); ?>">
                                        <?php echo ( get_sub_field( 'font_awesome_code' ) ); ?>
                                    </a>
                                </li>

                            <?php endwhile; ?>
                        </ul>
                    </nav>
                <?php endif; ?>
                <div class="copyright-container">
                    <?php if ( have_rows( 'legal_links', 'option' ) ) : ?>
                        <nav class="legal-nav" role="navigation" aria-label="Legal Links">
                            <ul>
                                <?php while ( have_rows( 'legal_links', 'option' ) ) : the_row(); ?>

                                    <li>
                                        <?php $link = get_sub_field( 'nav_link' ); ?>
                                        <a href="<?php echo esc_url( $link['url'] ); ?>" target="<?php echo esc_attr( $link['target'] ); ?>" aria-label="<?php echo esc_attr( $link['title'] ); ?>">
                                            <?php echo esc_html( $link['title'] ); ?>
                                        </a>
                                    </li>

                                <?php endwhile; ?>
                            </ul>
                        </nav>
                    <?php endif; ?>
                    <p class="copyright">&copy; <?php echo date( 'Y' ); ?> <?php echo ( get_field( 'site_name', 'option' ) ) ? get_field( 'site_name', 'option' ) : ''; ?> All rights reserved. <?php echo ( get_field( 'footer_copyright_tagline', 'option' ) ) ? '<strong>' . get_field( 'footer_copyright_tagline', 'option' ) . '</strong>' : ''; ?></p>
                </div><!--.copyright-container -->

            </div><!--.social-footer-nav-container -->          
        </div> <!--.container -->
    </footer>
	<?php wp_footer(); ?>
    <?php do_action( 'rcd_body_after' ); ?>
</body>
</html>
